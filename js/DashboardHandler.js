
class DashboardHandler {
    showMenuSelector() {
        var self = this;

        var app = KartaBase.getApp();
        var selected = app.shadowRoot.querySelector( "#main-selector" );
        selected.removeAttribute( "hidden" );
        selected.style.display = "block";

        var list = app.shadowRoot.querySelector( "karta-campaign-list" );
        list.setAttribute( "hidden", "" );
        list.style.display = "none";        
    }

    hideMenuSelector(){
        var self = this;

        var app = KartaBase.getApp();
        var selected = app.shadowRoot.querySelector( "#main-selector" );
        selected.setAttribute( "hidden", "" );
        selected.style.display = "none";

        var list = app.shadowRoot.querySelector( "karta-campaign-list" );
        var tamp = list.activeCampaigns;

        list.activeCampaigns = [];
        list.activeCampaigns = tamp;

        list.removeAttribute( "hidden" );
        list.style.display = "block";
    }

    setUpInitialDashboard() {
        var self = this;

        var nav = KartaBase.getDashboardNavBottom();
        // nav.selected = "stat";
    }

    setUpDrawer() {
        var self = this;

        var app = KartaBase.getApp();
        var switchCampaign = app.shadowRoot.querySelector( "#campaign-menu" );
        var switchCampaign2 = app.shadowRoot.querySelector( "#campaign-name-menu" );
        var hidden = true;
        var switchCampaignFn = function(){
            if(hidden) self.hideMenuSelector();
            else self.showMenuSelector();

            hidden = !hidden;
        };

        switchCampaign.onclick = switchCampaignFn;
        switchCampaign2.onclick = switchCampaignFn;

        var list = app.shadowRoot.querySelector( "karta-campaign-list" );
        var selectList = list.shadowRoot.querySelector( "#campaign-list" );
        selectList.addEventListener( "selected-item-changed", function(ev, obj){
            // alert("selected-item-changed");
            KartaBase.startUI();
            var detail = ev.detail;
            var campaign = detail.value || "";

            if( campaign.length <= 0 ) return;
            console.log("campaign name:", campaign);
            KartaBase.setUpCampaign( campaign );
            selectList.clearSelection();
            app.$.drawer.close();

            // request
            var stat = KartaBase.getDashboardStatRequest();
            stat.request();
            // end of request

            // clear previously selected data live
            var live = KartaBase.getDashboardLiveRequest();
            live.clearLiveMap();
            if (window.liveMap) delete window.liveMap;
            // end of clear previously selected data live

            // clear previously selected data heatmap
            var heatmap = KartaBase.getDashboardHeatmapRequest();
            heatmap.clearHeatMap();
            if (window.heatMap) delete window.heatMap;
            // end of clear previously selected data heatmap

            self.showMenuSelector();
            hidden = !hidden;
            // KartaBase.finishUI(); // commented 21 sep; finish ui on stat request
        } );
    }

    setSelectedMCD() {
        var bcheck = localStorage.getItem( "mcd_info" );
        if( !bcheck ) return false;

        var campaigns = JSON.parse( bcheck );
        var app = KartaBase.getApp();
        app.campaignList = campaigns;

        KartaBase.setUpCampaign( campaigns[0] );
        return true;
    }

    setSelectedBAPP() {
        var bcheck = localStorage.getItem( "bapp_info" );
        if( !bcheck ) return false;

        var campaigns = JSON.parse( bcheck );
        var app = KartaBase.getApp();
        app.campaignList = campaigns;

        console.log( "list: ", bcheck, campaigns );
        KartaBase.setUpCampaign( campaigns[0] );
        return true;
    }

    // setSelectedDriver() {
    //     var bcheck = localStorage.getItem( "driver_info" );
    //     if( !bcheck ) return false;

    //     var campaigns = JSON.parse( bcheck );
    //     var app = KartaBase.getApp();
    //     app.campaignList = campaigns;

    //     console.log( "list: ", bcheck, campaigns );
    //     KartaBase.setUpCampaign( campaigns[0] );
    //     return true;
    // }


    setDefaultCampaign() {
        var bcheck = localStorage.getItem( "campaign_list" );
        if( !bcheck ) return false;

        var campaigns = JSON.parse( bcheck );
        var app = KartaBase.getApp();
        app.campaignList = campaigns;
        console.log("Cek bcheck", campaigns);

        var menu = app.shadowRoot.querySelector( "#campaign-menu" );
        menu.style.display = "block";

        var switchCampaign = app.shadowRoot.querySelector( "#campaign-menu" );
        var switchCampaign2 = app.shadowRoot.querySelector( "#campaign-name-menu" );
        if( campaigns.length <=1 ) {
            menu.style.display = "none";
            switchCampaign.onclick = undefined;
            switchCampaign2.onclick = undefined;
        }

        console.log( "num campaigns: ", campaigns.length );

        KartaBase.setUpCampaign( campaigns[0] );

        // 28 sep header always displayed, copy from karta-app _pageChanged
        var app = KartaBase.getApp();
        app.isdashboard = true;
        var stylepage = app.shadowRoot.querySelector("iron-pages");
        stylepage.style.marginTop = "-40px";
        console.log("dashboard flag ","1");
        // end of 28 sep header always displayed, copy from karta-app _pageChanged

        return true;
    }

    response() {
        var self = this;
        var req = KartaBase.getRequest();
        req.addEventListener( "response", function(resp){
            var detail = resp.detail.response;
            var mode = detail.data.post.ajax_mode || "";
            // console.log("logout ", mode)
            switch( mode ){
                case "jwt-login":
                    self.getJwtLogin( detail );
                    break;

                case "jwt-authenticate":
                    self.getJwtAuthenticate( detail );
                    break;
                case "jwt-logout":
                    self.getJwtLogout( detail );
                    break;
                    
                case "get-client-dashboard-heatmap":
                    var heatreq = KartaBase.getDashboardHeatmapRequest();
                    heatreq.response( detail );
                    break;

                case "get-client-dashboard-stat":
                    var dashreq = KartaBase.getDashboardStatRequest();
                    dashreq.response( detail );
                    break;

                case "get-client-mcd":
                    self.getClientMCD( detail );
                    break;

                case "get-client-bapp":
                    self.getClientBAPP( detail );
                    break;

                case "get-client-driver":
                    self.getClientDriver( detail );
                    break;

                case "get-detail-driver":
                    self.getDetailDriver( detail );
                    break;

                case "get-client-campaign":
                    self.getClientCampaign( detail );
                    break;

                case "get-client-gallery":
                    self.getClientGallery( detail );
                    break;

                default: 
                    break;
            }            
        });
    }

    getJwtLogin( detail ) {
        var self = this;

        //localStorage.setItem( "salt_response", JSON.stringify( detail ) );
        localStorage.setItem( "salt", detail.data.salt );
        localStorage.setItem( "version", detail.data.version.version );
        console.log( "salt response: ", detail.data );
        console.log( "version response: ", detail.data.version.version );
    }

    getJwtAuthenticate( detail ) {
        var self = this;

        localStorage.setItem( "jwt_token", detail.data.jwt_token );
        console.log( "auth response: ", detail.data );

        //parse, success only
        var statuscode = detail.code;
        var userauth = detail.data.all_status;
        console.log("aut resp", userauth);
        // var adsasia = detail.data.user_type.email;
        // console.log("mail data", adsasia);
        if (statuscode == 200) {
            if (userauth == "OK") {
                var usertype = detail.data.user_type.is_client;  //&& adsasia == "shopeebali@karta.asia"
                if (usertype == 1 ) {

                    KartaBase.startUI();
                    var login = document.querySelector( "karta-login" );
                    var app = document.querySelector( "karta-app" );
                    login.style.display = "none";
                    app.style.display = "block";

                    var body = document.querySelector( "body" );
                    body.style.background= "#f8f8f8";

                    var karta_user_id = detail.data.user_type.user_id;
                    var karta_client_id = detail.data.user_type.client_id;
                    var jwt_token = detail.data.jwt_token;
                    var karta_user_email = detail.data.user_type.email;
                    console.log("loginData",karta_user_id, karta_client_id, karta_user_email);
                    login.dispatchEvent( new CustomEvent("login-success", {
                        detail: {hello: "world"}
                    } ) );

                    localStorage.setItem( "karta-user-id", karta_user_id );
                    localStorage.setItem( "karta-client-id", karta_client_id );
                    localStorage.setItem( "jwt_token", jwt_token );
                    localStorage.setItem( "karta-user-email", karta_user_email ); //set last email used //demokarta

                    var handler = KartaBase.getDashboardHandler();
                    handler.request();

                    var pages = KartaBase.getPages();
                    pages.selected = DEFAULT_PAGE;

                    var menus = KartaBase.getMenus();
                    menus.querySelector("a[name="+ DEFAULT_PAGE +"]").click()

                    setTimeout(function(){
                        var handler = KartaBase.getDashboardHandler();
                        handler.request();    
                    }, 1000);
                    
                    KartaBase.finishUI();
                }
                else {
                    
                    document.dispatchEvent(new CustomEvent('paper-snackbar-notify', {
                        bubbles: true, 
                        composed: true,
                        detail: { 
                            message: 'User is not client!', 
                            
                        }
                    }));
                    KartaBase.finishUI();
                }
            }
            else { 

                document.dispatchEvent(new CustomEvent('paper-snackbar-notify', {
                    bubbles: true, 
                    composed: true,
                    detail: { 
                        message: detail.data.all_message, 
                        //targetTitle: 'Magic Link',
                        //targetUrl: '#'
                    }
                }));
                KartaBase.finishUI();
            }
        }
        else { //400 etc
            //alert(statuscode);
            document.dispatchEvent(new CustomEvent('paper-snackbar-notify', {
                bubbles: true, 
                composed: true,
                detail: { 
                    message: statuscode, 
                    //targetTitle: 'Magic Link',
                    //targetUrl: '#'
                }
            }));
            KartaBase.finishUI();
            
        }
        
    }

    getJwtLogout( detail ) {
        var self = this;

        var karta_user_email = localStorage.getItem( "karta-user-email" );
        localStorage.clear(); //clear all items (all key-values)
        // localStorage.removeItem( "karta-user-id" );
        // localStorage.removeItem( "karta-client-id" );
        // localStorage.removeItem( "jwt_token" );
        localStorage.setItem( "karta-user-email", karta_user_email ); //set last email used
        localStorage.setItem("flagLiveHeatmap", "flagon");
        console.log( "logout response: ", detail );



        // clear all campaign data from prev login
        // clear object in window; cleanup info/filter in live
        if ( "liveMap" in window  ) {
            if( "mainCluster" in window ) {
                mainCluster.clearMarkers();
                delete window.mainCluster;
            }
            liveMap.remove(); // remove map
            delete window.liveMap;
        }
        if ( "heatMap" in window  ) {
            heatMap.remove();
            delete window.heatMap;
        }
        // clear mainActiveDriver and map
        if ( "mainActiveDrivers" in window ) {
            mainActiveDrivers = null;
            // delete mainActiveDrivers;
        }
        // end of clear all campaign data from prev login



        var app = KartaBase.getApp();
        var live = app.shadowRoot.querySelector("karta-dashboard-live");
        var info = live.shadowRoot.querySelector("#campaign-info");
        info.style.display = "none";
        var icon = live.shadowRoot.querySelector("#icon-search");
        icon.style.display = "none";
        var filter = live.shadowRoot.querySelector("#filter-dialog");
        filter.style.display = "none";
        // end of clear object in window; cleanup info/filter in live



        //return to login page
        var card = document.querySelector( " karta-login " );
        var app = document.querySelector( " karta-app " );
        var body = document.querySelector( " body " );
        body.style.background= "#0F405B";

        card.style.display = "block";
        app.style.display = "none";

        /*
        Velocity( app, "fadeOut", {
            duration:1000,
            complete: function(){
                Velocity( card, "fadeIn", {
                    duration:1000,
                    complete: function(){
                        // card.style.display = "block";
                        // app.style.display = "none";
                    }
                } );
            }
        } );
        */
        //     document.addEventListener('deviceready', function() {
        // var success = function(status) {
        //     // alert('Message: ' + status);
        // };
        // var error = function(status) {
        //     // alert('Error: ' + status);
        // };
        // window.CacheClear(success, error);
        // });
        // // window.location.reload(true);
        window.location.reload(true);
        
        // set pages&selected
        var pages = KartaBase.getPages();
        pages.selected = "clientapp";
        var login = document.querySelector( "karta-login" );
        login.addEventListener( "login-success", function(ev){
            // alert("login-success");
            // // di-comment all temp 24 sep
            // var pages = KartaBase.getPages();
            // pages.selected = DEFAULT_PAGE; // orig
            // // pages.selected = "clientapp";
            
            var app = KartaBase.getApp();
            var selector = app.shadowRoot.querySelector( "#main-selector" );
            selector.selected = "dashboard";
            // end of di-comment all temp 24 sep

            // uncomment temp
            var pages = KartaBase.getPages();
            pages.selected = DEFAULT_PAGE;
            pages.selected = "clientapp";

            // di-copy dari KartaApp _setUpListener login-success
            // 19 sep comment
            var app = KartaBase.getApp();
            app.shadowRoot.querySelector( "a[name="+ DEFAULT_PAGE +"]" ).click();
            // var pages = KartaBase.getPages();
            // pages.selected = DEFAULT_PAGE;
            history.replaceState({ page:"dashboard", nav:"stats" }, "Karta Client", "/dashboard");
            // pages.selected = DEFAULT_PAGE;
            var handler = KartaBase.getAppHandler();
            handler.handleRoute( self, DEFAULT_PAGE );
            self.versionName = KartaBase.getVersion();
            self.clientPhoto = localStorage.getItem("client_logo_url");
            // console.log("photo ", self.clientPhoto);
            var valueHeader = KartaBase.getApp();
            console.log("campaignName start",valueHeader.campaignName);
            // uncomment temp

            // KartaBase.finishUI();
        } );
        
        // get salt , copy dari karta-login
        var req = KartaBase.getRequest();
        var headers = {
            "X-Authorization": KartaConfig.apiAuthorization,
        };

        var postdata = {
            ajax_mode: "jwt-login",
        };

        console.log( "login post: ", postdata );

        req.url = KartaConfig.apiGetClientJwtLogin();
        req.method = "GET";
        req.headers = headers;
        req.params = postdata;

        req.generateRequest();
    }

    getClientMCD( detail ) {

        var mcdInfo = detail.data.mcd_info || {};
        var mcdId = mcdInfo.mcd_id || -1;

        localStorage.setItem( "mcd_" + mcdId, JSON.stringify( mcdInfo ) );
        
        KartaBase.setUpMCDInfo( mcdInfo );
        KartaBase.finishUI();
    }

    getClientBAPP( detail ) {
        var bappInfo = detail.data.bapp_info || {};
        var bappId = bappInfo.bapp_id || -1;

        localStorage.setItem( "bapp_" + bappId, JSON.stringify( bappInfo ) );
        
        KartaBase.setUpBAPPInfo( bappInfo );
        KartaBase.finishUI();
    }

    getClientDriver( detail ) {
        //thisss!!!
        // var driverInfo = detail.data.driver_info || {};
        // var driverId = driverInfo.driver_id || -1;

        // localStorage.setItem( "driver_" + driverId, JSON.stringify( driverInfo ) );
        
        // KartaBase.setUpDriverInfo( driverInfo );

        //new: do nothing, balikin halaman tablenya aja
        console.log("getClientDriver");
        KartaBase.finishUI();
    }

    getDetailDriver( detail ) {
        var driverDetailInfo = detail.data.biker_detail[0] || {}; console.log("driverDetailInfo: ", driverDetailInfo);
        var driverFlatDetail = detail.data.biker_flat_detail || {}; console.log("driverFlatDetail: ", driverFlatDetail);
        var driverEvaluation = detail.data.biker_evaluation || {}; console.log("driverEvaluation: ", driverEvaluation);
        var driverId = driverDetailInfo.biker_id || -1;

        localStorage.setItem( "driver_detail_" + driverId, JSON.stringify( driverDetailInfo ) );
        
        KartaBase.setUpDriverDetailInfo( driverDetailInfo, driverFlatDetail, driverEvaluation );

        KartaBase.finishUI();
    }

    getClientCampaign( detail ) {
        var self = this;
        console.log("detailLog", detail);

        var campaigns = detail.data.campaign_list || [];
        var campaign_areas = detail.data.campaign_area || [];
        var client_logo_url = detail.data.client_logo_url || [];
        localStorage.setItem( "campaign_list", JSON.stringify( campaigns ) );
        localStorage.setItem( "campaign_area", JSON.stringify( campaign_areas ) );
        localStorage.setItem( "client_logo_url",  client_logo_url  );
        self.setDefaultCampaign();
    }

    getClientGallery( detail ) {
        KartaBase.setUpGallery(detail);
        console.log("getClientGallery");
        KartaBase.finishUI();
    }

    request() {
        var self = this;

        if( self.setDefaultCampaign() ) return;
        var req = KartaBase.getRequest();
        console.log("get request");
        var headers = {
            "X-Authorization": KartaConfig.apiAuthorization,
        };

        var userId = localStorage.getItem( "karta-user-id" );
        var clientId = localStorage.getItem( "karta-client-id" );
        if(!clientId) {
            console.error( "No client id: found... Retrying..." );
            setTimeout(function(){
                userId = localStorage.getItem( "karta-user-id" );
                clientId = localStorage.getItem( "karta-client-id" );

            }, 3000);
            return;
        }

        var postdata = {
            ajax_mode: "get-client-campaign",
            user_id: userId,
            client_id: clientId,
        };

        req.url = KartaConfig.apiGetClientCampaign();
        req.method = "GET";
        req.headers = headers;
        req.params = postdata;
        
        req.generateRequest();
    }
}
