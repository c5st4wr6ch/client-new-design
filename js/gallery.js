window.addEventListener('load', function() {
	// https://www.codesd.com/item/drag-in-the-middle-to-open-the-panel-with-jquery-mobile.html
	var gallery = KartaBase.getPage("gallery");
}, false);

document.addEventListener('init', function(event) {
	console.log(event.target);
	if (event.target.matches('#galleryPage')) {
		// ons.notification.alert('galleryPage is initiated.');

		document.getElementById("gallery-image-overlay").addEventListener("click", function(){
	        console.log('click gallery overlay close');
	        hideAndShowGalleryImage('','');
	    });
	    $( ".gallery-slider-close-btn" ).off("click").on( "click", function( event ) {
	        hideAndShowGalleryImage('','');
	    });
	}
}, false);

// toggle image subcampaign from click, lsslide; copy from map.js
function hideAndShowGalleryImage(url, gallery_name){
    // change image
    var galleryImg = document.querySelector("#gallery-image-img");
    galleryImg.src = url || "";
    var galleryName = document.querySelector("#gallery-image-name");
    galleryName.innerHTML = gallery_name || "";

    var details = $('#gallery-image');
    var courtain = $('#gallery-image-overlay');
    
    if(courtain.hasClass('inactive')){
        courtain.removeClass('inactive');
        courtain.addClass('active');
        details.removeClass('close');
    }
    else{
        courtain.removeClass('active');
        courtain.addClass('inactive');
        details.addClass('close');
    }
}