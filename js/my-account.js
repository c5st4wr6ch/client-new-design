// push page campaign detail
document.addEventListener('show', function(event) {
    var page = event.target;

    if (page.id === 'myAccountPage') {
        setTimeout(function(){
            // https://stackoverflow.com/a/44194619
            var cardDetails = page.querySelectorAll('ons-card');
            cardDetails.forEach(function(card) {
                card.addEventListener('click', function() {
                    var driverId = this.id.replace("campaign-push-page-", ""); // console.log(driverId);
                    document.querySelector('#kartaNavigator').pushPage('html/campaign-detail.html', {data: {id: driverId}});
                });
            });
        }, 1000);
        
    } else if (page.id === 'campaignDetailPage') {
        // page.querySelector('ons-toolbar .center').innerHTML = page.data.id; // SET CAMPAIGN NAME FROM getCampaignDetail
        getCampaignDetail(page.data.id); // GET CAMP DETAIL FROM STORAGE
    }
});

// request detail
function getCampaignDetail(id) {

    var account_list_campaign = localStorage.getItem( "account_list_campaign" );
    var campaigns = JSON.parse(account_list_campaign);

    var campaign = campaigns.filter(obj => {
        return obj.id == id;
    })

    // display campaign detail
    var page = document.querySelector('#campaignDetailPage');
    var toolbar = page.querySelector('ons-toolbar .center');
    toolbar.innerHTML = campaign[0].name;

    var campaignDetailImage = document.querySelector('#campaign-detail-photo');
    $(campaignDetailImage).attr("src", campaign[0].photo_url);

    var campaignDetailDrivers = document.querySelector('#campaign-detail-drivers');
    campaignDetailDrivers.innerHTML = campaign[0].driver + ' drivers';
    var campaignDetailArea = document.querySelector('#campaign-detail-area');
    campaignDetailArea.innerHTML = campaign[0].area;
    var campaignDetailPeriod = document.querySelector('#campaign-detail-period');
    campaignDetailPeriod.innerHTML = moment(campaign[0].start_campaign, "YYYY-MM-DD").format( "DD MMM YYYY" ) +
        ' - ' + moment(campaign[0].end_campaign, "YYYY-MM-DD").format( "DD MMM YYYY" );

    var campaignDetailDescription = document.querySelector('#campaign-detail-description');
    campaignDetailDescription.innerHTML = campaign[0].description;

    // IF SUBC EXISTS
    var divCampaignDetailSubcampaigns = document.querySelector('#div-campaign-detail-subcampaigns');
    var campaignDetailSubcampaigns = document.querySelector('#campaign-detail-subcampaigns');
    if (campaign[0].subcampaign_list.length > 0) {
        // show, display
        divCampaignDetailSubcampaigns.style.display = "block";
        var subs = '<div id="lightSlider">';
        for (var i = 0; i < campaign[0].subcampaign_list.length; i++) {
            subs += '<div>' +
                '<img src="' + KartaConfig.assetsUrl + campaign[0].subcampaign_list[i].artwork_photo + '" alt="subcampaign image" height="120" width="80"' +
                ' onclick="hideAndShowAccountSubcampaignImage(\'' + KartaConfig.assetsUrl + campaign[0].subcampaign_list[i].artwork_photo + '\',\''+campaign[0].subcampaign_list[i].subcampaign_name+'\')" />' +
                '</div>';
                // campaign[0].subcampaign_list[i].subcampaign_name +
        }
        subs += "</div>";
        campaignDetailSubcampaigns.innerHTML = subs;

        // $("#lightSlider").lightSlider({
        //     onSliderLoad: function(el) {
        //         console.log(el);
        //         // el.lightGallery({
        //         //     selector: '#lightSlider  .lslide'
        //         // });
        //     }
        // });

        // img onclick not working
        $( "#lightSlider" ).find( ".lslide" ).each(function( index ) {
            console.log( index + ": " + $( this ).text() );
            // $( this ).addEventListener("click", function(){
            //     console.log(this);
            // });

            // https://stackoverflow.com/a/22015946
            $( this ).on("click touchstart", function(){

                var imgsub = $(this).find('img');
                var imgsubclick = $(imgsub).attr("onclick");
                console.log(imgsubclick); // alert("click");
                eval(imgsubclick);
            });
        });
    }
    else {
        // hide
        campaignDetailSubcampaigns.innerHTML = "";
        divCampaignDetailSubcampaigns.style.display = "none";
    }

    // set cmp image&name to panel
    var acctCampaignImg = document.querySelector("#account-campaign-image-img");
    acctCampaignImg.src = campaign[0].photo_url || "";
    var acctCampaignName = document.querySelector("#account-campaign-name");
    acctCampaignName.innerHTML = campaign[0].name || "";

    // show/hide campaign image
    $("#campaign-detail-photo").click(hideAndShowAcctCampaignImage);
    $("#account-campaign-overlay").click(hideAndShowAcctCampaignImage);
    $( ".account-campaign-slider-close-btn" ).off("click").on( "click", function( event ) {
        hideAndShowAcctCampaignImage();
    });

    document.getElementById("account-subcampaign-overlay").addEventListener("click", function(){
        console.log('click overlay close');
        hideAndShowAccountSubcampaignImage('','');
    });
    $( ".account-subcampaign-slider-close-btn" ).off("click").on( "click", function( event ) {
        hideAndShowAccountSubcampaignImage();
    });

    // img onclick not working
    $('.List li').on('click touchstart', function() {
        $('.Div').slideDown('500');
    });
}

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function toggleMenu() {
    document.getElementById("myDropdown").classList.toggle("show");
}

var createConfirmDialog = function() {
    var dialog = document.getElementById('logout-confirm-dialog');

    if (dialog) {
        dialog.show();
    } else {
        ons.createElement('confirm-dialog.html', { append: true })
            .then(function(dialog) {
                dialog.show();
            });
    }
};

var hideConfirmDialog = function() {
    document
        .getElementById('logout-confirm-dialog')
        .hide();
};

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}

var logout = function() {
    // save last login email
    var karta_user_email = localStorage.getItem( "karta-user-email" );
    localStorage.clear(); //clear all items (all key-values)
    if (karta_user_email != 'demo@karta.asia') { // if demokarta then clear; get new email
        localStorage.setItem( "karta-user-email", karta_user_email ); //set last email used
    }
    localStorage.setItem("flagLiveHeatmap", "flagon");

    // redirect to login
    window.location = "login.html";
};

// toggle image campaign from click cmp img; copy from map.js
function hideAndShowAcctCampaignImage(){
    var details = $('#account-campaign-image');
    var courtain = $('#account-campaign-overlay');
    
    if(courtain.hasClass('inactive')){
        courtain.removeClass('inactive');
        courtain.addClass('active');
        details.removeClass('close');
    }
    else{
        courtain.removeClass('active');
        courtain.addClass('inactive');
        details.addClass('close');
    }
}

// toggle image subcampaign from click, lsslide; copy from map.js
function hideAndShowAccountSubcampaignImage(url, sub_name){
    // change image
    var subcampaignImg = document.querySelector("#account-subcampaign-image-img");
    subcampaignImg.src = url || "";
    var subcampaignName = document.querySelector("#account-subcampaign-name");
    subcampaignName.innerHTML = sub_name || "";

    var details = $('#account-subcampaign-image');
    var courtain = $('#account-subcampaign-overlay');
    
    if(courtain.hasClass('inactive')){
        courtain.removeClass('inactive');
        courtain.addClass('active');
        details.removeClass('close');
    }
    else{
        courtain.removeClass('active');
        courtain.addClass('inactive');
        details.addClass('close');
    }
}