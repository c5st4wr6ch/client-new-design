    
    var KartaConfig = {
        // apiUrl: "http://localhost/titanlight/index.php",

        apiUrl: "http://karta.id/titanlight/index.php",
        assetsUrl: "http://karta.id/titan/",
        apiAuthorization: "87943F424IO876568A765E2L7D6N548",
        s3Url: "https://s3-ap-southeast-1.amazonaws.com",
        reportMCDUrl: "http://karta.id/titan/export/mcd?",
        reportBAPPUrl: "http://karta.id/titan/client-bapp/export/",
        copyrightText: (new Date()).getFullYear() +" © PT Karta Indonesia Global",
        version: "1.0.0",

        // demokarta
        driverImg: "images/profile_blue_karta.png",
        campaignImg: "images/bensin.jpg",
        // end of demokarta

        DEFAULT_PAGE: "dashboard", 
        SLOW_CONNECTION_TIMEOUT: 3000,
        apiGetS3ClientMCD: function( brandName, version ){
            var self = this;
            var fname = brandName.replace(/\ /gi,"_");
            return self.s3Url + "/karta-reports/mcd/" + fname + "_"+ version +".pdf";
        },
        apiGetS3ClientBAPP: function( brandName, version ){
            var self = this;
            var fname = brandName.replace(/\ /gi,"_");
            return self.s3Url + "/karta-reports/bapp/" + fname + "_"+ version +".pdf";
        },
        apiGetClientS3GeoLive: function( campaignId ){
            var self = this;
            return self.s3Url + "/karta-geolive/geolive/campaign_" + campaignId + "/geolive-"+ campaignId +".json";
        },

        apiGetClientJwtLogin: function(){
            var self = this;
            return self.apiUrl + "/api/mobile/auth/jwt-login";
        },
        apiGetClientJwtAuthenticate: function(){
            var self = this;
            return self.apiUrl + "/api/mobile/auth/jwt-authenticate";
        },
        apiGetClientJwtLogout: function(){
            var self = this;
            return self.apiUrl + "/api/mobile/auth/jwt-logout";
        },
        apiGetClientDashboardStat: function(){
            var self = this;
            return self.apiUrl + "/api/mobile/dashboard/get-client-dashboard-stat";
        },
        apiGetClientDashboardHeatmap: function(){
            var self = this;
            return self.apiUrl + "/api/mobile/dashboard/get-client-dashboard-heatmap";
        },
        apiGetClientMCD: function(){
            var self = this;
            return self.apiUrl + "/api/mobile/reports/get-client-mcd";
        },
        apiGetClientBAPP: function(){
            var self = this;
            return self.apiUrl + "/api/mobile/reports/get-client-bapp";
        },
        //added temp, test
        apiGetClientDriver: function(){
            var self = this;
            // return self.apiUrl + "/api/mobile/dashboard/get-client-driver";
            //return self.apiUrl + "/api/admin/get-biker-server-side-without-odo-photo";
            // return "http://karta.id/titan/index.php" + "/api/admin/get-biker-server-side-without-odo-photo";
            // return "http://karta.id/titan" + "/api/mobile/driver-get-list";
            return self.apiUrl + "/api/mobile/driver-get-list";
            // return "http://localhost/titanlight/index.php/api/mobile/driver-get-list";
        },
        apiGetClientDriverNonActive: function(){ // removed/replaced driver
            var self = this;
            return self.apiUrl + "/api/mobile/get-client-biker-non-active";
        },
        //end of added temp, test
        apiGetDetailDriver: function(){
            var self = this;
            return self.apiUrl + "/api/mobile/driver/get-detail-driver";
        },
        apiGetClientCampaign: function(){
            var self = this;
            return self.apiUrl + "/api/mobile/campaign/get-client-campaign";
        },
        apiGetClientDashboardHeatmapRange: function(){
            var self = this;
            return self.apiUrl + "/api/mobile/dashboard/get-client-dashboard-choropleth-daterange";
        },
        apiGetGallery: function(){
            var self = this;
            return self.apiUrl + "/api/campaign/get-campaign-gallery-image";
        },
        apiGetDashboardStat: function(){
            var self = this;
            return self.apiUrl + "/api/campaign/get-dashboard-stats-info";
        },
        apiGetDashboardLive: function(){
            var self = this;
            return self.apiUrl + "/api/campaign/get-driver-live-route";
        },
        apiGetClientProfile: function(){
            var self = this;
            return self.apiUrl + "/api/mobile/client/profile";
        }
    }
