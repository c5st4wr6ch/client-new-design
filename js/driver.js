// push page driver detail
document.addEventListener('show', function(event) {
	var page = event.target;

	if (page.id === 'driverPage') {

        // // set info to map&driver
        // var campaignName = localStorage.getItem( "karta-campaign-name" );
        // var campaignUrl = localStorage.getItem( "karta-campaign-url" );

        // var contentCmp = document.querySelector( "#driver-ctnt-campaign-info" );
        // contentCmp.innerHTML = campaignName || "N/A";
        // var campaignImg = document.querySelector("#driver-img-campaign");
        // campaignImg.src = campaignUrl || "";

        // var subcampaignObj = localStorage.getItem("subcampaigns");
        // var subcampaigns = JSON.parse(subcampaignObj);
        // if (subcampaigns.length > 0) { // existed subcampaign
        //     var contentSubCmp = document.querySelector( "#driver-ctnt-subcampaign-info" );
        //     contentSubCmp.innerHTML = "All subcampaigns";
        // }
        // else {
        //     var divInfoSubCmp = document.querySelector( "#div-driver-info-subcampaign" );
        //     divInfoSubCmp.style.display = "none";
        //     var divFilterSubCmp = document.querySelector( "#div-driver-filter-subcampaign" );
        //     divFilterSubCmp.style.display = "none";
        // }

        setTimeout(function(){ 
    		// alert('driverPage'); console.log('### driverPage ###', page.querySelectorAll('ons-card'));

    		// https://stackoverflow.com/a/44194619
    		var cardDetails = page.querySelectorAll('ons-card');
    		cardDetails.forEach(function(card) {
    			card.addEventListener('click', function() {
    				var driverId = this.id.replace("driver-push-page-", ""); // console.log(driverId);
    				document.querySelector('#kartaNavigator').pushPage('html/driver-detail.html', {data: {id: driverId}});
    			});
    		});
        }, 1000);
	} else if (page.id === 'driverDetailPage') {
		// page.querySelector('ons-toolbar .center').innerHTML = page.data.id;
		getDriverDetail(page.data.id);
	}
});



// reset filter
var doFilterReset = document.querySelector("#driver-filter-reset");
doFilterReset.addEventListener('click', function(event){
    // get previously selected campaign
    var clientId = localStorage.getItem("karta-client-id");
    var campaignId = localStorage.getItem("karta-campaign-id");
    var subcampaignId = localStorage.getItem("karta-subcampaign-id") || -1;
    var subcampaigns = localStorage.getItem( "subcampaigns" );
    var subcampaignsObj = JSON.parse(subcampaigns);
    var userId = localStorage.getItem("karta-user-id");

    // clear subcampaign
    if (subcampaignsObj.length > 0) {
        localStorage.setItem("karta-subcampaign-id", -1);
        localStorage.setItem("karta-subcampaign-name", "All subcampaigns");

        // chg selected to all subs
        var select = document.getElementById("select-driver-filter-subcampaign");
        select.value = -1;
    }
    // end of clear subcampaign

    // chg image to cmp image & set sub all camp; copy from filter apply
    var campaignImage = document.getElementById( "driver-img-campaign" );
    var campaignImageSlider = document.getElementById( "driver-campaign-image-img" );
    var campaignUrl = localStorage.getItem( "karta-campaign-url" );
    campaignImage.src = campaignUrl || "";
    campaignImageSlider.src = campaignUrl || "";

    var campaignName = localStorage.getItem( "karta-campaign-name" );
    var contentCmp = document.querySelector( "#driver-ctnt-campaign-info" );
    var campaignNameSlider = document.getElementById( "driver-campaign-name" );
    contentCmp.innerHTML = campaignName || "N/A";
    campaignNameSlider.innerHTML = campaignName || "N/A";

    if (subcampaignsObj.length > 0) { // existed subcampaign
        var contentSubCmp = document.querySelector( "#driver-ctnt-subcampaign-info" );
        contentSubCmp.innerHTML = "All subcampaigns";
    }
    // chg image to cmp image & set sub all camp; copy from filter apply
    
    $("#driver-filter-select").slideReveal("hide"); // HIDE FILTER PANEL

    var apiAuthorization = KartaConfig.apiAuthorization;
    $.ajax({
        url: KartaConfig.apiGetClientDriver(),
        data: {
            campaign_id: campaignId,
            client_id: clientId,
            user_id: userId
        },
        method: "GET",
        dataType: 'json',
        headers: {"X-Authorization": KartaConfig.apiAuthorization},
        success: function(detail, status){
            console.log('detail: ', detail);

            // set number of drivers
            $('#driver-number').html(detail.length);

            $('#pagination-container').pagination({
                dataSource: detail,
                callback: function(data, pagination) {
                    var html = cardTemplating(data);
                    $('#data-container').html(html);
                }
            });
        },
        error: function(){
            alert('There was an error loading the data: get-client-driver');
        }
    });
});



// request detail
// DashboardHandler.getDetailDriver(detail)
function getDriverDetail(id) {
	var userId = localStorage.getItem( "karta-user-id" );
    var clientId = localStorage.getItem( "karta-client-id" );
    var campaignId = KartaBase.getCampaignId();
    var bikerId = id;

	$.ajax({
		url: KartaConfig.apiGetDetailDriver(),
		data: {
			campaign_id: campaignId,
			client_id: clientId,
			user_id: userId,
			biker_id: bikerId
		},
		method: "POST",
		dataType: 'json',
		headers: {"X-Authorization": KartaConfig.apiAuthorization},
		success: function(detail, status){
			console.log('detail: ', detail);

			var driverDetailInfo = detail.data.biker_detail[0] || {}; console.log("driverDetailInfo: ", driverDetailInfo);
	        var driverFlatDetail = detail.data.biker_flat_detail || {}; console.log("driverFlatDetail: ", driverFlatDetail);
	        var driverEvaluation = detail.data.biker_evaluation || {}; console.log("driverEvaluation: ", driverEvaluation);
	        var driverId = driverDetailInfo.biker_id || -1;

	        localStorage.setItem( "driver_detail_" + driverId, JSON.stringify( driverDetailInfo ) );

            var campaignEmail = localStorage.getItem( "karta-user-email" );
            var photo = KartaConfig.assetsUrl + driverDetailInfo.photo;
            if (campaignEmail == 'demo@karta.asia') { // demokarta
                photo = 'images/profile_blue_karta.png';
            }

			// 3 parts: card profile, campaign detail, list evaluation
			// display image&info
			var driverDetailImage = document.querySelector('#driver-detail-photo');
			$(driverDetailImage).attr("src", photo);
			var driverDetailName = document.querySelector('#driver-detail-name');
            // 18 sep change biker name; copy from map.js
            var bikname = '';
            var arr_name = driverDetailInfo.biker_name.split(" ");
            if (arr_name.length > 0) {
                for (var j=0; j<arr_name.length; j++) {
                    var sub = arr_name[j].substring(0, 1);
                    bikname += sub + ".";
                }
            }
            bikname = bikname.toUpperCase();
            // end of 18 sep change biker name; copy from map.js
			driverDetailName.innerHTML = bikname;
			var driverDetailMerk = document.querySelector('#driver-detail-vehicle');
			driverDetailMerk.innerHTML = driverDetailInfo.merk_kendaraan;
			var driverDetailKm = document.querySelector('#driver-detail-km'); // not to count here: see flat
			driverDetailKm.innerHTML = driverFlatDetail[0].km_final + ' km';



			/************************************* draw table flat *************************************/
            var div_biker_flat_detail = document.querySelector('#biker_flat_detail'); console.log("biker_flat_detail: ", div_biker_flat_detail);
            if (!driverFlatDetail) {
                div_biker_flat_detail.style.display = 'none';
            }
            else {
                div_biker_flat_detail.style.display = 'block';

                var control = document.querySelector('#campaign-flat-card'); console.log("control: ", control);
                var campaign_flat_template = function(){
                    // return '<tr class="odd gradeX">'+
                    //         '<td>'+
                    //             '{{nth_seq}}'+
                    //         '</td>'+
                    //         '<td>'+
                    //             '{{start_register}}'+
                    //         '</td>'+
                    //         '<td>'+
                    //             '{{expiry_register}}'+
                    //         '</td>'+
                    //         '<td>'+
                    //             '{{km_distance}}'+
                    //         '</td>'+
                    //         '<td>'+
                    //             '{{is_achieve_target}}'+
                    //             '{{completed_date}}'+
                    //         '</td>'+
                    //     '</tr>';

                    return '<div class="card">' +
                            '<div class="card__content">' +
                                '<p>Stage {{nth_seq}}</p>' +
                                '<hr class="driver-detail-hr" />' +
                                '<div class="row">' +
                                    '<div class="col-xs-4 driver-detail-color-gray">' +
                                        'Start' +
                                    '</div>' +
                                    '<div class="col-xs-8 text-right driver-detail-color-blue">' +
                                        '{{start_register}}' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-xs-4 driver-detail-color-gray">' +
                                        'Expiry' +
                                    '</div>' +
                                    '<div class="col-xs-8 text-right driver-detail-color-blue">' +
                                        '{{expiry_register}}' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-xs-4 driver-detail-color-gray">' +
                                        'Distance' +
                                    '</div>' +
                                    '<div class="col-xs-8 text-right driver-detail-color-blue">' +
                                        '{{km_distance}}' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-xs-4 driver-detail-color-gray">' +
                                        'Status' +
                                    '</div>' +
                                    '<div class="col-xs-8 text-right driver-detail-color-blue">' +
                                        '{{is_achieve_target}} {{completed_date}}'+
                                        // '{{status}}'+
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
                }
                // {{(bfd.km_distance/1000)|number_format}} Km
                // {% if bfd.is_achieve_target == 0%}Target Not Achieved Yet{% endif %}
                // {% if bfd.completed_date %}<br>Completed at {{bfd.completed_date}}{% endif %}

                var tmp = '';
                var ext = '';

                console.log('driverFlatDetail: ', driverFlatDetail);
                var iLen = driverFlatDetail.length;
                for(var i=0;i<iLen;i++){
                    tmp = campaign_flat_template();
                    tmp = tmp.replace(/{{nth_seq}}/gi, driverFlatDetail[i].nth_seq);
                    var start_date = moment(driverFlatDetail[i].start_register, "YYYY-MM-DD").format( "DD MMM YYYY" );
                    tmp = tmp.replace(/{{start_register}}/gi, start_date);
                    var expiry_date = moment(driverFlatDetail[i].expiry_register, "YYYY-MM-DD").format( "DD MMM YYYY" );
                    tmp = tmp.replace(/{{expiry_register}}/gi, expiry_date);
                    var km_distance = driverFlatDetail[i].km_distance/1000 + " km";
                    tmp = tmp.replace(/{{km_distance}}/gi, km_distance);
                    var is_achieve_target = (driverFlatDetail[i].is_achieve_target != 1)? "Target Not Achieved Yet" : "";
                    tmp = tmp.replace(/{{is_achieve_target}}/gi, is_achieve_target);
                    // var completed_date = (driverFlatDetail[i].completed_date)? "<br>Completed at " + driverFlatDetail[i].completed_date : "";
                    var completed_date = (driverFlatDetail[i].is_achieve_target == 1)? "Completed at " + moment(driverFlatDetail[i].expiry_register, "YYYY-MM-DD").format( "DD MMM YYYY" ) : "";
                    // not displayed if finished; chg completed to asd
                    tmp = tmp.replace(/{{completed_date}}/gi, completed_date);
                    tmp = tmp.replace(/{{status}}/gi, driverFlatDetail[i].status);
                    ext += tmp;
                }
                control.innerHTML = ext;
            }
            console.log("finished table flat");



            /************************************* draw table evaluasi *************************************/
            var div_biker_evaluation = document.querySelector('#odometer_not_broken'); console.log("biker_evaluation: ", div_biker_evaluation);
            if (!(driverDetailInfo.is_odometer_broken !=1)) {
                div_biker_evaluation.style.display = 'none';
            }
            else {
                div_biker_evaluation.style.display = 'block';

                var control = document.querySelector('#evaluation-tbody'); console.log("control: ", control);

                var tmp = '';
                var ext = '';

                var iLen = driverEvaluation.length;
                for(var i=0;i<iLen;i++){
                    var tr1 = '<tr>';
                    var td1_template = function(){
                        return '<td>'+
                                '<input type="checkbox" class="checkboxes" value="" name="check" />'+
                            '</td>';
                    };
                    var td2_template = function(){
                        return '<td>'+
                                '{{evaluation_date}}'+
                            '</td>';
                    };
                    var td3_template = function(){
                        if (driverEvaluation[i].bracket_url) {
                            return '<td>'+
                                    '<ons-icon id="bracket{{id}}" icon="fa-check" onclick="hideAndShowDriverDetailEvalImage(\'bracket\',{{id}},\'{{bracket_url}}\',\'{{evaluation_date}}\')"></ons-icon>'+
                                '</td>';
                        }
                        else {
                            return '<td>'+
                                    '<ons-icon icon="fa-close"></ons-icon>'+
                                '</td>';
                        }
                    }
                    var td4_template = function(){
                        if (driverEvaluation[i].seal_url) {
                            return '<td>'+
                                    '<ons-icon id="seal{{id}}" icon="fa-check" onclick="hideAndShowDriverDetailEvalImage(\'seal\',{{id}},\'{{seal_url}}\',\'{{evaluation_date}}\')"></ons-icon>'+
                                '</td>';
                        }
                        else {
                            return '<td>'+
                                    '<ons-icon icon="fa-close"></ons-icon>'+
                                '</td>';
                        }
                    }
                    var td5_template = function(){
                        if (driverEvaluation[i].odometer_url) {
                            return '<td>'+
                                    '<ons-icon id="odo{{id}}" icon="fa-check" onclick="hideAndShowDriverDetailEvalImage(\'odometer\',{{id}},\'{{odometer_url}}\',\'{{evaluation_date}}\')"></ons-icon>'+
                                '</td>';
                        }
                        else {
                            return '<td>'+
                                    '<ons-icon icon="fa-close"></ons-icon>'+
                                '</td>';
                        }
                    }
                    var tr2 = '</tr>';

                    var td1 = td1_template();
                    var td2 = td2_template();
                    td2 = td2.replace(/{{evaluation_date}}/gi, moment(driverEvaluation[i].evaluation_date, "YYYY-MM-DD").format( "DD MMM YYYY" ));
                    var td3 = td3_template();
                    td3 = td3.replace(/{{id}}/gi, driverEvaluation[i].id);
                    td3 = td3.replace(/{{bracket_url}}/gi, driverEvaluation[i].bracket_url);
                    td3 = td3.replace(/{{evaluation_date}}/gi, driverEvaluation[i].evaluation_date);
                    var td4 = td4_template();
                    td4 = td4.replace(/{{id}}/gi, driverEvaluation[i].id);
                    td4 = td4.replace(/{{seal_url}}/gi, driverEvaluation[i].seal_url);
                    td4 = td4.replace(/{{evaluation_date}}/gi, driverEvaluation[i].evaluation_date);
                    var td5 = td5_template();
                    td5 = td5.replace(/{{id}}/gi, driverEvaluation[i].id);
                    td5 = td5.replace(/{{odometer_url}}/gi, driverEvaluation[i].odometer_url);
                    td5 = td5.replace(/{{evaluation_date}}/gi, driverEvaluation[i].evaluation_date);
                    ext += tr1;
                    // ext += td1; // hide the checkbox; no use
                    ext += td2;
                    ext += td3;
                    ext += td4;
                    ext += td5;
                    ext += tr2;
                }
                control.innerHTML = ext;
            } 
            console.log("finished table evaluation");
		},
		error: function(){
			alert('There was an error loading the data: get-driver-detail');
		}
	});
}

// // convert eval param to obj
// function toObjectEvaluation() {
//     var target = target;

//     showDriverDetailPopover(target, eval);
// }

function setDetailEvalPopover(type, id, url, date) {
    // change info popover
    var evalType = document.getElementById('driver-detail-type');
    evalType.innerHTML = type;
    var evalId = document.getElementById('driver-detail-id');
    evalId.value = id;
    var evalUrl = document.getElementById('driver-detail-url');
    $(evalUrl).attr("src", url);
    var evalDate = document.getElementById('driver-detail-date');
    evalDate.innerHTML = date;
}

// , type, id, url, date
var showDriverDetailPopover = function(target, type, id, url, date) {
    var popover = document.getElementById('driver-detail-popover');

    if (popover) {
        setDetailEvalPopover(type, id, url, date);
        popover.show(target);
    } else {
        ons.createElement('driver-detail-popover.html', { append: true })
            .then(function(dialog) {
                setDetailEvalPopover(type, id, url, date);
                popover.show(target);
            });
    }

    // if (!popover) {
    //     ons.createElement('driver-detail-popover.html', { append: true });
    //     popover = document.getElementById('driver-detail-popover');
    // }
    // popover.show(target);
    // // setDetailEvalPopover(type, id, url, date);
};

// window.showPopover = showPopover;

var hideDriverDetailPopover = function() {
    document
        .getElementById('driver-detail-popover')
        .hide();
};
// window.hidePopover = hidePopover;

function setupFilterDriver() {
    var campaignName = localStorage.getItem( "karta-campaign-name" );
    var campaignUrl = localStorage.getItem( "karta-campaign-url" );



    // set campaign info; see ClientDashboardStats response; add from map opt selected
    var campaignId = localStorage.getItem("karta-campaign-id");
    var campaignNameObj = document.querySelector( "#select-driver-filter-campaign" );
    // var campaignName = campaignNameObj.options[campaignNameObj.selectedIndex].text;
    var campaignName = localStorage.getItem("karta-campaign-name");
    var contentCmp = document.querySelector( "#driver-ctnt-campaign-info" ); 
    contentCmp.innerHTML = campaignName || "N/A";
    // localStorage.setItem("karta-campaign-name", campaignName);
    
    var subcampaignId = localStorage.getItem("karta-subcampaign-id");
    var subcampaignNameObj = document.querySelector( "#select-driver-filter-subcampaign" );
    // var subcampaignName = subcampaignNameObj.options[subcampaignNameObj.selectedIndex].text;
    var subcampaignName = localStorage.getItem("karta-subcampaign-name");
    var contentSubcmp = document.querySelector( "#driver-ctnt-subcampaign-info" ); console.log(contentSubcmp);
    contentSubcmp.innerHTML = subcampaignName || "N/A";
    // localStorage.setItem("karta-subcampaign-name", subcampaignName);
    // end of set campaign info; see ClientDashboardStats response; add from map opt selected
    
    var driverCampaignImg = document.querySelector("#driver-img-campaign");
    // driverCampaignImg.src = campaignUrl || "";



    // apply filter
    var doFilterApply = document.querySelector("#driver-filter-apply");
    doFilterApply.addEventListener('click', function(event){
        // alert('doFilterApply');

        var userId = localStorage.getItem( "karta-user-id" );
        var clientId = localStorage.getItem( "karta-client-id" );
        var campaignId = KartaBase.getCampaignId();

        // get data from filter: selected mode & campaign/sub in filter
        var fdcId = document.querySelector( "select#select-driver-filter-campaign" );
        var campaignId = fdcId[fdcId.selectedIndex].value || -1;
        var fdscId = document.querySelector( "select#select-driver-filter-subcampaign" );
        var subcampaignId = (fdscId[fdscId.selectedIndex])? fdscId[fdscId.selectedIndex].value : -1;
        localStorage.setItem("karta-campaign-id", campaignId);
        localStorage.setItem("karta-subcampaign-id", subcampaignId);

        console.log('campaignId: ', campaignId, ', subcampaignId: ', subcampaignId);
        // // sample lazada
        // var campaignId = 346;
        // var subcampaignId = 89;



        // set campaign info; see ClientDashboardStats response
        var campaignNameObj = document.querySelector( "#select-driver-filter-campaign" );
        var campaignName = campaignNameObj.options[campaignNameObj.selectedIndex].text;
        var contentCmp = document.querySelector( "#driver-ctnt-campaign-info" ); 
        contentCmp.innerHTML = campaignName || "N/A";
        localStorage.setItem("karta-campaign-name", campaignName);

        var subcampaignNameObj = document.querySelector( "#select-driver-filter-subcampaign" );
        var subcampaignName = subcampaignNameObj.options[subcampaignNameObj.selectedIndex].text;
        var contentSubcmp = document.querySelector( "#driver-ctnt-subcampaign-info" ); 
        contentSubcmp.innerHTML = subcampaignName || "N/A";
        localStorage.setItem("karta-subcampaign-name", subcampaignName);
        // end of set campaign info; see ClientDashboardStats response



        var campaign_image_name = document.querySelector( "#driver-campaign-name" );
        if (subcampaignNameObj.length > 0) {
            campaign_image_name.innerHTML = campaignName + '<br />' + subcampaignName;
        }
        else {
            campaign_image_name.innerHTML = campaignName;
        }



        // set subcampaign image to filter AND panel
        var campaignImage = document.getElementById( "driver-img-campaign" );
        var campaignImageSlider = document.getElementById( "driver-campaign-image-img" );
        if (subcampaignId > 0) {
            var subcampaigns = localStorage.getItem( "subcampaigns" );
            var subcampaignsObj = JSON.parse(subcampaigns);
            var subcampaign = subcampaignsObj.find(function (obj) { return obj.id == subcampaignId; });
            campaignImage.src = "http://karta.id/titan/" + subcampaign.artwork_photo || "#";
            campaignImageSlider.src = "http://karta.id/titan/" + subcampaign.artwork_photo || "#";
        }
        else { // case: select sub, then select all
            // reset to image campaign
            var campaignUrl = localStorage.getItem( "karta-campaign-url" );
            campaignImage.src = campaignUrl || "";
            campaignImageSlider.src = campaignUrl || "";
        }
        // end of set subcampaign image to filter AND panel

        // copy from karta.js, add filter
        $.ajax({
            url: KartaConfig.apiGetClientDriver(),
            data: {
                campaign_id: campaignId,
                client_id: clientId,
                user_id: userId
            },
            method: "GET",
            dataType: 'json',
            headers: {"X-Authorization": KartaConfig.apiAuthorization},
            success: function(detail, status){
                console.log('detail: ', detail);

                // filter by sub id
                var detailFiltered = [];
                if (subcampaignId > 0) {
                    // https://stackoverflow.com/a/13964186
                    var drawActiveDrivers = mainActiveDrivers.filter(function( obj ) {
                        return obj.subcampaign_id == subcampaignId;
                    });
                    // detailFiltered = drawActiveDrivers;

                    for(var i=0; i<detail.length; i++) {
                        // https://stackoverflow.com/a/35398031
                        // if exist in draw then insert main
                        if (drawActiveDrivers.find(x => x.biker_id === detail[i].biker_id)) {
                            detailFiltered.push(detail[i]);
                        }
                    }
                }
                else {
                    detailFiltered = detail;
                }
                console.log('detailFiltered: ', detailFiltered);
                
                // set number of drivers
                $('#driver-number').html(detailFiltered.length);

                $('#pagination-container').pagination({
                    dataSource: detailFiltered,
                    callback: function(data, pagination) {
                        var html = cardTemplating(data);
                        $('#data-container').html(html);
                    },
                    afterPaging: function(e) {
                        console.log('[afterPaging]: ', e);
                        setUpDriverPagingListener();
                    }
                });
            },
            error: function(){
                alert('There was an error loading the data: get-client-driver');
            }
        });

        $("#driver-filter-select").slideReveal("hide"); // HIDE FILTER PANEL
    });
}

// https://nnattawat.github.io/slideReveal/ Overlay Panel
function setupDriverFilterPanelListener() {
    $("#driver-filter-select").slideReveal({
        trigger: $("#driver-icon-search"),
        position: "right",
        push: false,
        overlay: true
    });
}

// set up listener go to detail on driver card click
function setUpDriverPagingListener() {
    console.log('[setUpDriverPagingListener]');
    var page = document.querySelector('#driverPage');
    setTimeout(function(){ 
        var cardDetails = page.querySelectorAll('ons-card');
        cardDetails.forEach(function(card) {
            card.addEventListener('click', function() {
                var driverId = this.id.replace("driver-push-page-", ""); // console.log(driverId);
                document.querySelector('#kartaNavigator').pushPage('html/driver-detail.html', {data: {id: driverId}});
            });
        });
    }, 800);
}

function setupDriverCampaignImageListener() {
    console.log('[[ setupDriverCampaignImageListener ]]');
    // off("click") to clear listener click
    // $( "#driver-img-campaign" ).off("click").on( "click", function( event ) {
    //     hideAndShowDriverCampaignImage();
    // });
    $( "#card-driver" ).off("click").on( "click", function( event ) {
        hideAndShowDriverCampaignImage();
    });
    $( "#driver-campaign-image-overlay" ).off("click").on( "click", function( event ) {
        hideAndShowDriverCampaignImage();
    });
    $( ".driver-slider-close-btn" ).off("click").on( "click", function( event ) {
        hideAndShowDriverCampaignImage();
    });
}

// toggle image campaign from click top left img, inside filter card; see map
function hideAndShowDriverCampaignImage() {
    var details = $('#driver-campaign-image');
    var courtain = $('#driver-campaign-image-overlay');
    
    if(courtain.hasClass('inactive')){
        courtain.removeClass('inactive');
        courtain.addClass('active');
        details.removeClass('close');
    }
    else{
        courtain.removeClass('active');
        courtain.addClass('inactive');
        details.addClass('close');
    }
}

// toggle image eval from click checkmark, inside driver detail table; see map
function hideAndShowDriverDetailEvalImage(type, id, url, date) {
    console.log("hideAndShowDriverDetailEvalImage");
    var details = $('#driver-eval-image');
    var courtain = $('#driver-eval-image-overlay');
    
    if(courtain.hasClass('inactive')){
        courtain.removeClass('inactive');
        courtain.addClass('active');
        details.removeClass('close');
    }
    else{
        courtain.removeClass('active');
        courtain.addClass('inactive');
        details.addClass('close');
    }

    setDetailEvalPopover(type, id, url, date);

    $( "#driver-eval-image-overlay" ).off("click").on( "click", function( event ) {
        setDetailEvalPopover(type, id, url, date);
        hideAndShowDriverDetailEvalImage('','','','');
    });
    $( ".driver-eval-slider-close-btn" ).off("click").on( "click", function( event ) {
        hideAndShowDriverDetailEvalImage();
    });
}