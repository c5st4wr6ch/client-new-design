// dashboard handler rewrite
document.querySelector('ons-tabbar').addEventListener('postchange', function(e) {
	var activeIndex = e.activeIndex;

	var campaignId = localStorage.getItem("karta-campaign-id");
    var clientId = localStorage.getItem("karta-client-id");
    var userId = localStorage.getItem("karta-user-id");

	// load page
	if (activeIndex == 0) { // map
		console.log('no reload map request');

		// set up info & filter; copy from driver.js
		// set campaign info; see ClientDashboardStats response; add from map opt selected
	    var campaignId = localStorage.getItem("karta-campaign-id");
	    var campaignNameObj = document.querySelector( "#select-filter-campaign" );
	    var campaignName = localStorage.getItem("karta-campaign-name");
	    var contentCmp = document.querySelector( "#map-ctnt-campaign-info" ); 
	    contentCmp.innerHTML = campaignName || "N/A"; // set info
	    campaignNameObj.value = campaignId; // set value filter
	    
	    var subcampaignId = localStorage.getItem("karta-subcampaign-id");
	    var subcampaignNameObj = document.querySelector( "#select-filter-subcampaign" );
	    var subcampaignName = localStorage.getItem("karta-subcampaign-name");
	    var contentSubcmp = document.querySelector( "#map-ctnt-subcampaign-info" );
	    contentSubcmp.innerHTML = subcampaignName || "N/A"; // set info
	    subcampaignNameObj.value = subcampaignId; // set value filter
	    // end of set campaign info; see ClientDashboardStats response; add from map opt selected



	    // set campaign image&name to panel swipe
        var campaign_image_ctnt = localStorage.getItem( "karta-campaign-url" );
        var campaign_image_img = document.querySelector( "#campaign-image-img" );
        campaign_image_img.src = campaign_image_ctnt || "#";
        var campaign_image_name = document.querySelector( "#campaign-name" );
        campaign_image_name.innerHTML = campaignName || "Campaign";
	}
	else if (activeIndex == 1) { // gallery
		// KartaBase.setUpGallery(detail);

		// request only if not in storage: request max 1 time
		if (localStorage.getItem("campaign_gallery") === null) {
			$.ajax({
				url: KartaConfig.apiGetGallery(),
				data: {
					campaign_id: campaignId,
					client_id: clientId,
					user_id: userId
				},
				method: "POST",
				dataType: 'json',
				headers: {"X-Authorization": KartaConfig.apiAuthorization},
				success: function(data, status){
					// console.log('data: ', data);

					// save to storage
					localStorage.setItem( "campaign_gallery", JSON.stringify( data.data.campaign_gallery ) );

					var onsrow = document.querySelector('#galleryData'); // console.log('onsrow: ', onsrow);
					onsrow.innerHTML = "";

					if (data.data.campaign_gallery.length > 0) {
						for (var i = 0; i < data.data.campaign_gallery.length; i++) {
							// data: image_url, caption
							var htm = '    <div class="gallery-wrapper" onclick="hideAndShowGalleryImage(\'' + data.data.campaign_gallery[i].image_url + '\',\''+ data.data.campaign_gallery[i].caption +'\')">' +
									'    	<div class="gallery-image">' +
									'	    	<img class="gallery-image-img" src="' + data.data.campaign_gallery[i].image_url + '" />' +
									'	    </div>' +
									'    	<div class="gallery-caption">' + data.data.campaign_gallery[i].caption + '</div>' +
									'    	<div class="gallery-img-over">' +
									'    		<img src="images/gallery/rounded.png">' +
									'    		<img class="instagram" src="images/gallery/instagram.png">' +
									'    	</div>' +
									'    </div>';

							$(onsrow).append(htm);
						}
					}
					else {
						// display empty gallery
                        var empty = document.getElementById("empty-gallery-content");
                        empty.style.display = "block";
					}
				},
				error: function(){
					alert('There was an error loading the data: get-campaign-gallery-image');
				}
			});
		}
	}
	else if (activeIndex == 2) { // drivers
		showLoadingModal();

		setupFilterDriver(); // set option&search by camp/sub
		setupDriverFilterPanelListener(); // set show/hide pane
		setupDriverCampaignImageListener(); // set show/hide image campaign

		// get newest selected camp
        var campaignId = localStorage.getItem("karta-campaign-id");
        var campaignName = localStorage.getItem("karta-campaign-name");
        var campaignEmail = localStorage.getItem( "karta-user-email" );
        if (campaignEmail == 'demo@karta.asia') { // demokarta
            campaignName = "Karta Demo";
        }
        var contentCmp = document.querySelector( "#driver-ctnt-campaign-info" );
        contentCmp.innerHTML = campaignName || "N/A";
		var campaignNameObj = document.querySelector( "#select-driver-filter-campaign" );
        campaignNameObj.value = campaignId;
		// end of get newest selected camp

		// get newest selected sub if exist //copy from map.js set info to map$driver
		var subcampaignObj = localStorage.getItem("subcampaigns");
        var subcampaigns = JSON.parse(subcampaignObj);
        if (subcampaigns.length > 0) { // existed subcampaign
            var subcampaignId = localStorage.getItem("karta-subcampaign-id");
	        var subcampaignName = localStorage.getItem("karta-subcampaign-name");

	        // first time load
	        if (subcampaignId == null) {
	        	subcampaignId = -1;
	        	subcampaignName = "All subcampaigns";
	        	localStorage.setItem("karta-subcampaign-id", subcampaignId);
	        	localStorage.setItem("karta-subcampaign-name", subcampaignName);
	        }
	        // end of first time load

	        var contentSubcmp = document.querySelector( "#driver-ctnt-subcampaign-info" );
	        contentSubcmp.innerHTML = subcampaignName || "N/A";
	        var subcampaignNameObj = document.querySelector( "#select-driver-filter-subcampaign" );
	        subcampaignNameObj.value = subcampaignId;
        }
        else {
            var divInfoSubCmp = document.querySelector( "#div-driver-info-subcampaign" );
            divInfoSubCmp.style.display = "none";
            var divFilterSubCmp = document.querySelector( "#div-driver-filter-subcampaign" );
            divFilterSubCmp.style.display = "none";
        }
        // end of get newest selected sub if exist //copy from map.js set info to map$driver



        // set campaign image&name to panel swipe
        var campaign_image_ctnt = localStorage.getItem( "karta-campaign-url" );
        var campaignImg = document.querySelector( "#driver-img-campaign" );
        var campaignImgSlider = document.querySelector( "#driver-campaign-image-img" );
        campaignImg.src = campaign_image_ctnt || "#";
        campaignImgSlider.src = campaign_image_ctnt || "#";
        var campaign_image_name = document.querySelector( "#driver-campaign-name" );
        campaign_image_name.innerHTML = campaignName || "Campaign";

        if (subcampaignId > 0) {
            var subcampaigns = localStorage.getItem( "subcampaigns" );
            var subcampaignsObj = JSON.parse(subcampaigns);
            var subcampaign = subcampaignsObj.find(function (obj) { return obj.id == subcampaignId; });
            campaignImg.src = "http://karta.id/titan/" + subcampaign.artwork_photo || "#";
            campaignImgSlider.src = "http://karta.id/titan/" + subcampaign.artwork_photo || "#";
        }
        else { // case: select sub, then select all
            // reset to image campaign
            var campaignUrl = localStorage.getItem( "karta-campaign-url" );
            campaignImg.src = campaignUrl || "";
            campaignImgSlider.src = campaignUrl || "";
        }



		$.ajax({
			url: KartaConfig.apiGetClientDriver(),
			data: {
				campaign_id: campaignId,
				client_id: clientId,
				user_id: userId
			},
			method: "GET",
			dataType: 'json',
			headers: {"X-Authorization": KartaConfig.apiAuthorization},
			success: function(detail, status){
				console.log('detail: ', detail);

				var subcampaignId = localStorage.getItem("karta-subcampaign-id") || -1;

				// filter by sub id
                var detailFiltered = [];
                if (subcampaignId > 0) {
                    // https://stackoverflow.com/a/13964186
                    var drawActiveDrivers = mainActiveDrivers.filter(function( obj ) {
                        return obj.subcampaign_id == subcampaignId;
                    });
                    // detailFiltered = drawActiveDrivers;

                    for(var i=0; i<detail.length; i++) {
                        // https://stackoverflow.com/a/35398031
                        // if exist in draw then insert main
                        if (drawActiveDrivers.find(x => x.biker_id === detail[i].biker_id)) {
                            detailFiltered.push(detail[i]);
                        }
                    }
                }
                else {
                    detailFiltered = detail;
                }
                console.log('detailFiltered: ', detailFiltered);

                // set number of drivers
                $('#driver-number').html(detailFiltered.length);

                $('#pagination-container').pagination({
                    dataSource: detailFiltered,
                    callback: function(data, pagination) {
                        var html = cardTemplating(data);
                        $('#data-container').html(html);
                    },
                    afterPaging: function(e) {
                    	console.log('[afterPaging]: ', e);
                    	setUpDriverPagingListener();
                    }
                });

                hideLoadingModal();
			},
			error: function(){
				alert('There was an error loading the data: get-client-driver');
				hideLoadingModal();
			}
		});

		$.ajax({
			url: KartaConfig.apiGetClientDriverNonActive(),
			data: {
				campaign_id: campaignId,
				client_id: clientId,
				user_id: userId
			},
			method: "GET",
			dataType: 'json',
			headers: {"X-Authorization": KartaConfig.apiAuthorization},
			success: function(detail, status){
				console.log('detail non active: ', detail);

				// show drive replacement
				var replace = document.getElementById("driver-replacement");
				if (detail.length > 0) {
					replace.style.display = "block";

					var tbody = document.getElementById("non_active_driver");
					for (var i=0; i<detail.length; i++) {
						// var row = '<tr>' +
						// 		'<td>' + (i+1) + '</td>' +
						// 		'<td>' + detail[i].biker_name + '</td>' +
						// 		'<td>' + detail[i].campaign_name + '</td>' +
						// 		'<td>' + detail[i].start_register + '</td>' +
						// 		'<td>' + detail[i].last_date + '</td>' +
						// 		'<td>' + '3' + '</td>' +
						// 		'<td>' + detail[i].display_km + '</td>' +
						// 		'<td>' + detail[i].status + '</td>' +
						// 	'</tr>';
						// $(row).appendTo(tbody);

						// 18 sep change biker name; copy from map.js
			            var bikname = '';
			            var arr_name = detail[i].biker_name.split(" ");
			            if (arr_name.length > 0) {
			                for (var j=0; j<arr_name.length; j++) {
			                    var sub = arr_name[j].substring(0, 1);
			                    bikname += sub + ".";
			                }
			            }
			            bikname = bikname.toUpperCase();
			            // end of 18 sep change biker name; copy from map.js

						var card = '<ons-row>' +
				'				<ons-card id="driver-push-page-' + detail[i].biker_id + '" class="list-driver">' +
				'			     	<img class="list-driver-image" src="#" alt="driver image" height="20" width="20">' +
				'			     	<h4 id="driver-' + i + '-nama">' + bikname + '</h4>' +
				'			     	<p><div class="img-list-driver">' +
				'			     		<img src="images/driver/driver-km.png" alt="km" height="16" width="16" />' +
				'			     	</div>' +
				'			     	<span id="driver-' + i + '-km">' +
										detail[i].display_km + ' km' +
				'			     	</span></p>' +
				'			     	<p><div class="img-list-driver">' +
				'			     		<img src="images/driver/driver-calendar.png" alt="period" height="16" width="16" />' +
				'			     	</div>' +
				'			     	<span id="driver-' + i + '-period">' +
										moment(detail[i].start_register, "YYYY-MM-DD").format( "DD MMM YYYY" ) + ' - ' + moment(detail[i].last_date, "YYYY-MM-DD").format( "DD MMM YYYY" ) +
				'			     	</span></p>' +
				'			     	<p><div class="img-list-driver">' +
				'			     		<img src="images/driver/driver-info.png" alt="info" height="16" width="16" />' +
				'			     	</div>' +
				'			     	<span id="driver-' + i + '-info">' +
										detail[i].status +
				'			     	</span></p>' +
				'			    </ons-card>' +
				'			</ons-row>';
						$(card).appendTo(tbody);
					}
				}
				else {
					replace.style.display = "none";
				}
			},
			error: function(){
				alert('There was an error loading the data: get-client-biker-non-active');
			}
		});
		
	}
	else if (activeIndex == 3) { // my account
		// request only if not in storage: request max 1 time
		if (localStorage.getItem("account_client_profile") === null) {
			showLoadingModal();
			$.ajax({
				url: KartaConfig.apiGetClientProfile(),
				data: {
					client_id: clientId,
					user_id: userId
				},
				method: "POST",
				dataType: 'json',
				headers: {"X-Authorization": KartaConfig.apiAuthorization},
				success: function(detail, status){
					console.log('detail: ', detail);

					var profile = detail.data.client_profile;
					var campaigns = detail.data.list_campaign;

					// save to storage
					localStorage.setItem( "account_client_profile", JSON.stringify( profile ) );
					localStorage.setItem( "account_list_campaign", JSON.stringify( campaigns ) );

					// display profile
					var companyName = profile.company_name;
					var clientLogo = profile.client_logo_small_url;
					var campaignEmail = localStorage.getItem( "karta-user-email" );
			        if (campaignEmail == 'demo@karta.asia') { // demokarta
			            companyName = "Karta Indonesia";
			            clientLogo = "images/karta-round.png";
			        }

					var accountImage = document.querySelector('.profile-image');
					$(accountImage).attr("src", clientLogo);
					var accountCompany = document.querySelector('.profile-name');
					accountCompany.innerHTML = companyName;

					var accountAddress = document.querySelector('#account-address');
					accountAddress.innerHTML = profile.office_address;
					var accountPhone = document.querySelector('#account-phone');
					accountPhone.innerHTML = profile.office_phone;
					var accountEmail = document.querySelector('#account-email');
					accountEmail.innerHTML = profile.email;
					var accountProvince = document.querySelector('#account-province');
					accountProvince.innerHTML = profile.client_province;
					var accountCity = document.querySelector('#account-city');
					accountCity.innerHTML = profile.client_city;
					var accountZipCode = document.querySelector('#account-zip-code');
					accountZipCode.innerHTML = profile.client_zipcode;

					// display campaigns
					var onsrow = document.querySelector('#campaignData'); // console.log('onsrow: ', onsrow);
					onsrow.innerHTML = "";

					for (var i = 0; i < campaigns.length; i++) {
						var campaignName = campaigns[i].name;
						if (campaignEmail == 'demo@karta.asia') { // demokarta
							campaignName = "Karta Demo";
						}
						var cardhtml = '<ons-row>' +
							'<ons-card id="campaign-push-page-' + campaigns[i].id + '" class="list-campaign">' +
								'<div class="list-campaign-image">' + moment(campaigns[i].end_campaign, "YYYY-MM-DD").format( "DD MMM YYYY" ) + '</div>' +
								'<h4 id="campaign-' + i + '-nama">' + campaignName + '</h4>' +
								'<div class="img-list-campaign">' +
									'<img src="images/my_campaign/My-Campign-Area.png" alt="driver" height="16" width="16" />' +
								'</div>' +
								'<span id="campaign-' + i + '-drivers">' +
									campaigns[i].driver +
								' Drivers</span>' +
								'<br />' +
								'<div class="img-list-campaign">' +
									'<img src="images/my_campaign/map-icon-in-my-campign-page.png" alt="area" height="16" width="16" />' +
								'</div>' +
								'<span id="campaign-' + i + '-area">' +
									campaigns[i].area +
								'</span>' +
								'<br />' +
								'<div class="img-list-campaign">' +
									'<img src="images/my_campaign/icons8-calendar.png" alt="period" height="16" width="16" />' +
								'</div>' +
								'<span id="campaign-' + i + '-period">' +
									moment(campaigns[i].start_campaign, "YYYY-MM-DD").format( "DD MMM YYYY" ) + ' - ' +
									moment(campaigns[i].end_campaign, "YYYY-MM-DD").format( "DD MMM YYYY" ) +
								'</span>' +
							'</ons-card>' +
						'</ons-row>';

						$(onsrow).append(cardhtml);
					}
					hideLoadingModal();
				},
				error: function(){
					alert('There was an error loading the data: get-client-profile');
					hideLoadingModal();
				}
			});
		}
		

	}
})

function cardTemplating(data) {
	console.log('cardTemplating data: ', data);

	var campaignEmail = localStorage.getItem( "karta-user-email" );

    var html = '<ons-col id="driverData">';
    $.each(data, function(index, item){
    	var photo = 'http://karta.id/titan/' + item.campaign_profile_photo;
		if (campaignEmail == 'demo@karta.asia') { // demokarta
			photo = 'images/profile_blue_karta.png';
		}

    	var row = '<ons-row>' +
'				<ons-card id="driver-push-page-' + item.biker_id + '" class="list-driver">' +
'			     	<img class="list-driver-image" src="' + photo + '" alt="driver image" height="20" width="20">' +
'			     	<h4 id="driver-' + index + '-nama">' + item.biker_name + '</h4>' +
'			     	<p><div class="img-list-driver">' +
'			     		<img src="images/driver/driver-km.png" alt="km" height="16" width="16" />' +
'			     	</div>' +
'			     	<span id="driver-' + index + '-km">' +
						item.display_km + ' km' +
'			     	</span></p>' +
'			     	<p><div class="img-list-driver">' +
'			     		<img src="images/driver/driver-calendar.png" alt="period" height="16" width="16" />' +
'			     	</div>' +
'			     	<span id="driver-' + index + '-period">' +
						moment(item.start_register, "YYYY-MM-DD").format( "DD MMM YYYY" ) + ' - ' + moment(item.expiry_register, "YYYY-MM-DD").format( "DD MMM YYYY" ) +
'			     	</span></p>' +
'			     	<p><div class="img-list-driver">' +
'			     		<img src="images/driver/driver-info.png" alt="info" height="16" width="16" />' +
'			     	</div>' +
'			     	<span id="driver-' + index + '-info">' +
						item.status +
'			     	</span></p>' +
'			    </ons-card>' +
'			</ons-row>';
		html += row;
	});
	html += '</ons-col>';

	return html;
}

// <ons-row>
// 	<ons-card id="campaign-push-page-' + campaigns[i].id + '" class="list-campaign">
//      	<img class="list-campaign-image" src="' + campaigns[i].photo_url + '" alt="campaign image" height="80" width="80">
//      	<h4 id="campaign-' + i + '-nama">' + campaigns[i].name + '</h4>
//      	<div class="img-list-campaign">
//      		<img src="images/my_campaign/campaign-driver.png" alt="driver" height="16" width="16" />
//      	</div>
//      	<span id="campaign-' + i + '-drivers">
// 			campaigns[i].driver
//      	</span>
//      	<br />
//      	<div class="img-list-campaign">
//      		<img src="images/my_campaign/campaign-area.png" alt="area" height="16" width="16" />
//      	</div>
//      	<span id="campaign-' + i + '-area">
// 			campaigns[i].area
//      	</span>
//      	<br />
//      	<div class="img-list-campaign">
//      		<img src="images/my_campaign/campaign-calendar.png" alt="period" height="16" width="16" />
//      	</div>
//      	<span id="campaign-' + i + '-period">
// 			campaigns[i].start_campaign + ' - ' + campaigns[i].end_campaign
//      	</span>
//     </ons-card>
// </ons-row>