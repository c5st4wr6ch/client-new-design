window.addEventListener('load', function () {
    console.log('login page loaded');
    // check if cordova is installed
    if( "cordova" in window ) {

    }
    else {
        // alert("cordova is not installed");
    }
    var self = this;

    

    $.ajax({
        url: KartaConfig.apiGetClientJwtLogin(),
        data: {
            ajax_mode: "jwt-login",
        },
        method: "GET",
        dataType: "json",
        headers: {"X-Authorization": KartaConfig.apiAuthorization},
        success: function( detail, status ){
            // save salt to localStorage; DashboardHandler getJwtLogin( detail )
            localStorage.setItem( "salt", detail.data.salt );
            localStorage.setItem( "version", detail.data.version.version );

            // set version to page
            var loginVersion = document.getElementById("login-version");
            loginVersion.innerHTML = 'v' + detail.data.version.version;
        },
        error: function(){
            alert("There was an error loading the data: get jwt-login");
        }
    });
});

// https://onsen.io/v2/api/js/ons-input.html#tutorial
var login = function() {
    // checkForEnter(); // check update version
    showLoadingModal();

    var username = document.getElementById('input-email').value;
    var password = document.getElementById('input-password').value;
    var salt = getUserSalt();
    var token = getUserToken();
    console.log('username: ', username, ', password: ', password, ', salt: ', salt, ', token: ', token);

    //demokarta
    if(username === 'demo@karta.asia' && password === 'kartademo')
    {
        username = 'spotify@karta.asia';
        password = 'spotifykarta';
        localStorage.setItem( "karta-user-email", "demo@karta.asia" ); //set last email used // demokarta
    }
    
    $.ajax({
        url: KartaConfig.apiGetClientJwtAuthenticate(),
        data: {
            ajax_mode: "jwt-authenticate",
            user_name: username.trim(),
            user_salt: salt,
            user_token: token
        },
        method: "POST",
        dataType: "json",
        headers: {"X-Authorization": KartaConfig.apiAuthorization},
        success: function( detail, status ){
            console.log('apiGetClientJwtAuthenticate detail: ', detail);
            // save login data to localStorage; DashboardHandler getJwtAuthenticate( detail )

            var jwt_token = detail.data.jwt_token;
            localStorage.setItem( "jwt_token", jwt_token );
            var statuscode = detail.code;
            var userauth = detail.data.all_status;

            if (statuscode == 200) {
                if (userauth == "OK") {
                    var usertype = detail.data.user_type.is_client;  //&& adsasia == "shopeebali@karta.asia"
                    if (usertype == 1 ) {

                        var karta_user_id = detail.data.user_type.user_id;
                        var karta_client_id = detail.data.user_type.client_id;
                        var karta_user_email = detail.data.user_type.email;
                        console.log("loginData",karta_user_id, karta_client_id, karta_user_email);

                        localStorage.setItem( "karta-user-id", karta_user_id );
                        localStorage.setItem( "karta-client-id", karta_client_id );
                        localStorage.setItem( "flagLogin", true );
                        localStorage.setItem( "karta-user-email", karta_user_email ); // set/replace last email used  // demokarta
                        localStorage.setItem( "jwt_token", jwt_token );

                        // get client campaign
                        getClientCampaign(karta_user_id, karta_client_id);
                    }
                    else {
                        ons.notification.alert('User is not client!');
                        hideLoadingModal();
                    }
                }
                else {
                    ons.notification.alert(detail.data.all_message);
                    hideLoadingModal();
                }
            }
            else  {
                ons.notification.alert(statuscode);
                hideLoadingModal();
            }
        },
        error: function(){
            alert("There was an error loading the data: get jwt-authenticate");
            ons.notification.alert('Incorrect username or password.');
            hideLoadingModal();
        }
    });


};

// copy from login-component.html
function getUserSalt() {
    var usersalt = localStorage.getItem( "salt" );
    return usersalt;
}

function getUserToken() {
    var self = this;
    
    var user_salt_request = this.getUserSalt();
    function hexStringToByte(str) {
        if (!str) {
            return new Uint8Array();
        }

        var a = [];
        for (var i = 0, len = str.length; i < len; i+=2) {
            a.push(parseInt(str.substr(i,2),16));
        }

        return new Uint8Array(a);
    }

    // var password_ = self.shadowRoot.querySelector( "#password" );
    var password_ = document.getElementById('input-password');
    var password = password_.value; // alert("password = " + password);
    if(password==='kartademo') //demokarta
    {
        password = 'spotifykarta';
    }

    var salt = user_salt_request;

    var secret = 'jQTNBpfuF/WY5E2cLRVoBD7RpG/GSuP635dMJTgQmDk=';
    var secretBytes = base64js.toByteArray( secret ); // UInt8
    var saltBytes = hexStringToByte( salt ); // UInt8
    var textBytes = aesjs.utils.utf8.toBytes(pad( Array(17).join( " " ), text ));

    /*
    1. var textBytes = aesjs.utils.utf8.toBytes(text);\
    2. base64_decode secret key [KEY]
    3. Hexa to UInt8 salt key [IV]
    4. var aesCbc = new aesjs.ModeOfOperation.cbc(key, iv);\
    5. var token = aesCbc.encrypt(textBytes); [TOKEN]
    6. Concat IV dengan TOKEN, lalu jadikan HEX
    */

    // 1.
    var text = password;
    var textBytes = aesjs.utils.utf8.toBytes(pad( Array(17).join( " " ), text )); //pad dulu

    // 2.
    var key = base64js.toByteArray(secret); //alert('key' + key); //OK, array

    // 3.
    var iv = saltBytes; //alert('iv = ' + iv); //OK, number int

    // 4.
    var aesCbc = new aesjs.ModeOfOperation.cbc(key, iv); //alert('aesCbc = ' + aesCbc); //OK, array

    // 5.
    var token = aesCbc.encrypt(textBytes); //alert('token = ' + token); //OK, array

    // 6.
    var saltBytes2 = saltBytes;
    var token2 = token;
    var tamp = new Uint8Array( saltBytes2.length + token2.length );
    tamp.set( saltBytes2 );
    tamp.set( token2, saltBytes2.length ); //index ke length
    var tamphex = toHexString(tamp); //alert('tamphex = ' + tamphex); //OK, hex
    return tamphex;
}

function toHexString(byteArray) {
    return Array.prototype.map.call(byteArray, function(byte) {
        return ('0' + (byte & 0xFF).toString(16)).slice(-2);
    }).join('');
}

function toByteArray (b64) {
    var i, l, tmp, placeHolders, arr
    var len = b64.length
    placeHolders = placeHoldersCount(b64)

    arr = new Array((len * 3 / 4) - placeHolders)

    l = placeHolders > 0 ? len - 4 : len

    var L = 0

    for (i = 0; i < l; i += 4) {
        tmp = (revLookup[b64.charCodeAt(i)] << 18) | (revLookup[b64.charCodeAt(i + 1)] << 12) | (revLookup[b64.charCodeAt(i + 2)] << 6) | revLookup[b64.charCodeAt(i + 3)]
        arr[L++] = (tmp >> 16) & 0xFF
        arr[L++] = (tmp >> 8) & 0xFF
        arr[L++] = tmp & 0xFF
    }

    if (placeHolders === 2) {
        tmp = (revLookup[b64.charCodeAt(i)] << 2) | (revLookup[b64.charCodeAt(i + 1)] >> 4)
        arr[L++] = tmp & 0xFF
    } else if (placeHolders === 1) {
        tmp = (revLookup[b64.charCodeAt(i)] << 10) | (revLookup[b64.charCodeAt(i + 1)] << 4) | (revLookup[b64.charCodeAt(i + 2)] >> 2)
        arr[L++] = (tmp >> 8) & 0xFF
        arr[L++] = tmp & 0xFF
    }

    return arr
}

function pad(pad, str, padLeft) {
    if (typeof str === 'undefined') 
        return pad;
    if (padLeft) {
        return (pad + str).slice(-pad.length);
    } else {
        return (str + pad).substring(0, pad.length);
    }
}

function getClientCampaign(user_id, client_id) {

    $.ajax({
        url: KartaConfig.apiGetClientCampaign(),
        data: {
            ajax_mode: "get-client-campaign",
            user_id: user_id,
            client_id: client_id
        },
        method: "GET",
        dataType: "json",
        headers: {"X-Authorization": KartaConfig.apiAuthorization},
        success: function( detail, status ){
            console.log('apiGetClientCampaign detail: ', detail);
            // save client campaign data to localStorage; DashboardHandler getClientCampaign( detail )

            var campaigns = detail.data.campaign_list || [];
            var campaign_areas = detail.data.campaign_area || [];
            var client_logo_url = detail.data.client_logo_url || [];
            localStorage.setItem( "campaign_list", JSON.stringify( campaigns ) );
            localStorage.setItem( "campaign_area", JSON.stringify( campaign_areas ) );
            localStorage.setItem( "client_logo_url",  client_logo_url  );
            setDefaultCampaign();
        },
        error: function(){
            alert("There was an error loading the data: get-client-campaign");
        }
    });
    
}

// DashboardHandler setDefaultCampaign
function setDefaultCampaign() {
    var bcheck = localStorage.getItem( "campaign_list" );
    if( !bcheck ) return false;

    var campaigns = JSON.parse( bcheck );

    console.log( "num campaigns: ", campaigns.length );

    setUpCampaign( campaigns[0] );

    return true;
}

// KartaBase setUpCampaign
function setUpCampaign( campaign ) {
    var self = this;

    if(!campaign) return;

    var campaignId = campaign.campaign_id || -1;
    var campaignName = campaign.campaign_name || -1;
    var campaignUrl = campaign.campaign_photo || -1;
    localStorage.setItem( "karta-campaign-id", campaignId );
    localStorage.setItem( "karta-campaign-name", campaignName );
    localStorage.setItem( "karta-campaign-url", campaignUrl );

    // redirect to dashboard
    setTimeout( function(){
        // window.location = "dashboard.html"; // add to history; will interfere with back button function
        window.location.replace("dashboard.html");
    }, 2000 );
}

// show/hide password, copy from client.login.tpl
// when password then EYE CLOSE; when text then EYE; e.g. google mail
function showPassword() {
    var x = document.getElementById("input-password");
    var eyes = document.getElementById("password-visible");
    if (x.type === "password") {
        x.type = "text";
        eyes.setAttribute("icon","fa-eye");
    } else {
        x.type = "password";
        eyes.setAttribute("icon","fa-eye-slash");
    }
}

// cordova function
// document.addEventListener("deviceready", onDeviceReady, false);
function onPageReady() {
    // alert('onPageReady');
    document.addEventListener("deviceready", onDeviceReady, false);
    document.addEventListener("volumedownbutton", onVolumeDownKeyDown, false);
    // document.addEventListener("backbutton", onBackKeyDown, false);
    document.addEventListener("backbutton", function(event) {
        var popup = document.querySelector("#popupExit");
        popup.show();
        
        event.preventDefault();
        event.stopPropagation();

        return false;
    }, false);
}

function onVolumeDownKeyDown() {
    // Handle the volume down button
    alert("onVolumeDownKeyDown");
    var popup = document.querySelector("#popupExit");
    popup.show();
}

function onDeviceReady() {
    alert('login onDeviceReady');
    // Register the event listener
    // document.addEventListener("backbutton", onBackKeyDown, false);







    document.addEventListener("backbutton", function(event) {
        alert("login onDeviceReady addEventListener backbutton");
        // var popup = document.querySelector("#popupExit");
        // popup.show();
        // alert(popup);
        
        event.preventDefault();
        event.stopPropagation();

        if (confirm('Are you sure you want to quit?')) {
            // Exit!
            navigator.app.exitApp();
        } else {
            // Do nothing!
        }

        return false;
    }, false);







    var versioncode = localStorage.getItem("version");
    // if(parseInt(versioncode)>parseInt(BuildInfo.versionCode))
    // {
    //     // window.open('http://bit.ly/kartaandroid', '_system');
    //     //  navigator.app.exitApp();
        
    //     var popup = document.querySelector("#popupVersion");
    //     popup.show();
    // }
    // alert('BuildInfo.versionCode    =' + BuildInfo.versionCode);
}

document.addEventListener("backbutton", function(event) {
    alert("login addEventListener backbutton");
    // var popup = document.querySelector("#popupExit");
    // popup.show();
    // alert(popup);
    
    event.preventDefault();
    event.stopPropagation();
    event.stopImmediatePropagation();

    // return false;
}, false);

// window.addEventListener("popstate", function(e) { // not working
//     alert("popstate");
//     //try e.preventDefault() or e.stopImmediatePropagation()
//     e.stopImmediatePropagation()
//     e.preventDefault();
// }, false);

// Handle the back button
// function onBackKeyDown() { // e
//     alert("onBackKeyDown");
//     e.preventDefault();
//     var popup = document.querySelector("#popupExit");
//     popup.show();
//     return false;
// }

function onExitClick() {
    var self = this;
    navigator.app.exitApp();
}

function onUpdateClick() {
    window.open('http://bit.ly/kartaandroid', '_system');
    navigator.app.exitApp();
}

function checkForEnter(){
    var versioncode = localStorage.getItem("version");
    if(parseInt(versioncode)>parseInt(BuildInfo.versionCode))
    {
        var popup = document.querySelector("#popupVersion");
        popup.open();
    }
}

var hidePopupExit = function() {
    var popup = document.querySelector("#popupExit");
    popup.hide();
};

var email_field = document.querySelector( "#input-email" );
var password_field = document.querySelector( "#input-password" );
var input_button = document.querySelector( "#login-button" );
email_field.onkeydown = function(e){
    if(e.keyCode == 13){
        input_button.click();
        // KartaBase.startUI(); //loader
    }
};
password_field.onkeydown = function(e){
    if(e.keyCode == 13){
        input_button.click();
        // KartaBase.startUI(); //loader
    }
};
var bcheck = localStorage.getItem("karta-user-email");
if ( bcheck ) {
    email_field.value = bcheck; //set last email used
}

// TODO history.replaceState({ page:"dashboard", nav:"stats" }, "Karta Client", "/dashboard")

function showLoadingModal() {
    var modal = document.getElementById("loading-modal");
    modal.show();
}

function hideLoadingModal() {
    var modal = document.getElementById("loading-modal");
    modal.hide();
}