var liveMap;
var geoArea;

// hideLoadingModal();
showLoadingModal();

// hide loading modal if request not working: 10s timeout
setTimeout( function(){
    hideLoadingModal();
}, 10000 );

window.addEventListener('load', function () {
    console.log('map load, request stat&live');
    // ons.notification.alert('mapPage is initiated using (1) window addEventListener load');
    // history.replaceState({}, "Karta Client", "dashboard.html") // hide

    setTimeout( function(){
        setUpMap();
        dragElement(document.getElementById("campaign-stat"));
    }, 1000 );

    

}, false);

function setUpMap() {
    // alert('setUpMap');
    var ACCESS_TOKEN = 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';
    // var mapPage = document.querySelector('ons-page#mapPage');
    // var divLiveMap = mapPage.querySelector('#live-map-container');
    var divLiveMap = document.querySelector('div#live-map-container'); console.log(divLiveMap);
    liveMap = L.map(divLiveMap).setView([-6.17511,106.8650395], 13);

    // var mapPage = $('ons-page#mapPage');
    // var divLiveMap = $(mapPage).find('#live-map-container');
    // var liveMap = L.map(divLiveMap).setView([-6.17511,106.8650395], 13);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + ACCESS_TOKEN, {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets'
        // accessToken: 'your.mapbox.access.token'
        // accessToken: ACCESS_TOKEN
    }).addTo(liveMap);







    // request stat & live
    var apiAuthorization = KartaConfig.apiAuthorization;

    var campaignId = localStorage.getItem("karta-campaign-id");
    var clientId = localStorage.getItem("karta-client-id");
    var userId = localStorage.getItem("karta-user-id");



    setTimeout( function(){
        requestDashboardStat();
        requestDashboardLive();
    }, 1000 );
    


    // hideLoadingModal();







    // filter live&heatmap onchange
    document.querySelector('ons-switch').addEventListener('change', function(e) {
        // alert('change'); console.log(e.value); // right/true=heatmap; left/false=live

        if (e.value == false) { // live
            showLoadingModal();

            // clear heatmap if any
            if (geojsonHeatmap) {
                geojsonHeatmap.clearLayers();
                // geojsonHeatmap = null; // not affected
            }
            if (geoHeatmap) {
                geoHeatmap.clearLayers();
                geoHeatmap = null;
            }

            // draw live
            var campaignId = localStorage.getItem("karta-campaign-id");
            var clientId = localStorage.getItem("karta-client-id");
            var userId = localStorage.getItem("karta-user-id");
            $.ajax({
                url: KartaConfig.apiGetDashboardLive(), // titanlight
                data: {
                    campaign_id: campaignId,
                    client_id: clientId,
                    user_id: userId
                },
                method: "POST",
                dataType: 'json',
                headers: {"X-Authorization": apiAuthorization},
                // jsonp: 'jsoncallback',
                // timeout: 5000,
                success: function(data, status){
                    // alert('live&heatmap onchange');

                    console.log('data: ', data);
                    // // save to localStorage; copy from DashboardLiveRequest
                    localStorage.setItem("biker_ids", JSON.stringify(data.data.biker_ids));
                    localStorage.setItem("biker_in_sse", JSON.stringify(data.data.biker_in_sse));
                    localStorage.setItem("campaign_area", JSON.stringify(data.data.campaign_area));
                    localStorage.setItem("live_bikers", JSON.stringify(data.data.live_bikers));
                    window.mainLiveDrivers = data.data.live_bikers;

                    setTimeout( function(){
                        drawCluster();
                        drawArea();

                        hideLoadingModal();
                    }, 2000 );
                },
                error: function(){
                    alert('There was an error loading the data: get-driver-live-route');
                }
            });

            // hide heatmap info
            $('#icon-legend').hide();
            $( "#heatmap-legend" ).hide();
            // hide empty if any
            var empty = document.getElementById("empty-heatmap-content");
            empty.style.display = "none";
        }
        else { // heatmap
            showLoadingModal();

            // ClientDashboardNavigation btnmode-heatmap
            // add leaflet, clear live map & area
            if (lfmarkers) {
                lfmarkers.clearLayers();
            }
            if(geoArea) {
                liveMap.removeLayer(geoArea);
                console.log("remove geoarea");
            }
            // end of add leaflet, clear live map & area

            // // clear heatmap if any
            // if (geojsonHeatmap) {
            //     geojsonHeatmap.clearLayers();
            // }
            // // end of clear heatmap if any

            var flagChoroplet = 'normal';
            var campaignId = localStorage.getItem("karta-campaign-id");
            var clientId = localStorage.getItem("karta-client-id");
            var userId = localStorage.getItem("karta-user-id");
            $.ajax({
                url: KartaConfig.apiGetClientDashboardHeatmap(),
                data: {
                    ajax_mode: "get-client-dashboard-heatmap",
                    user_id: userId,
                    client_id: clientId,
                    campaign_id: campaignId
                },
                method: "POST",
                dataType: 'json',
                headers: {"X-Authorization": apiAuthorization},
                success: function(data, status){
                    // alert('live&heatmap onchange');
                    console.log('data: ', data);

                    choroplethFeatureCollection = []; // clear
                    cfc = []; // clear

                    // heatmap must be >=14 days from start campaign date
                    var min_date = data.data.min_date;
                    var biweek_date = data.data.biweek_date;
                    localStorage.setItem("biweek_date", biweek_date);

                    let today = moment(new Date());
                    var biweek_date = moment(biweek_date, "YYYY-MM-DD");
                    if ( moment(today).isAfter(moment( biweek_date )) ) { // if today > campaign end date
                        // display heatmap
                        choropleths = data.data.choropleth_areas || undefined;
                        if(!choropleths) {
                            // console.log("cek c");
                            choropleths= data.data.choropleth_area_daterange || undefined;
                            flagChoroplet = "range";
                            if(!choropleths)
                            {
                                console.log("cek d");
                            // KartaBase.finishUI();
                                return false;
                            }

                        }
                        if(flagChoroplet === "normal")
                        {
                            var a = data.data.choropleth_area_daily;
                            var lengthDaily = Object.keys(a).length;
                            // console.log("data daily",Object.keys(a).length);
                            localStorage.setItem("min_date_daily",Object.keys(a)[0]);
                            localStorage.setItem("max_date_daily",Object.keys(a)[lengthDaily- 1]);
                            // console.log("Object value", Object.keys(a)[0], lengthDaily);

                        }
                        console.log("heatmap data ", data);
                        
                        localStorage.removeItem( "choropleths_"+campaignId);
                        localStorage.setItem( "choropleths_"+campaignId, JSON.stringify( data ) );
                        
                        console.log("locals item",choropleths);
                        
                        var i, iLen = choropleths.length;
                        // var heatmap = KartaBase.getDashboardHeatmap();
                        for( i=0;i<iLen;++i ) {
                            console.log('choropleths[', i, ']: ', choropleths[i]);
                            
                            // totalWeight = choropleths[i].total_weight;
                            // totalKmdistance = choropleths[i].total_km_campaign;
                            var obj = self._createChoroplethArea( choropleths[i],flagChoroplet);

                            // heatmap.appendChild( obj );

                            //var tamp = self._createLfLayer( map, json[i] ); console.log('tamp[', i, ']: ', tamp);
                            // areaLayers.push( tamp );
                        }
                        window.areaLayers = areaLayers;

                        setTimeout( function(){
                            console.log("visualizeIt");

                            console.log('choroplethFeatureCollection before _cfc: ', choroplethFeatureCollection);
                            self._cfc();
                            self._drawChoropleth();
                            
                            hideLoadingModal();
                        }, 3000 );
                    }
                    else {
                        // display empty heatmap
                        var empty = document.getElementById("empty-heatmap-content");
                        empty.style.display = "block";
                    }
                },
                error: function(){
                    alert('There was an error loading the data.: get-client-dashboard-heatmap');
                }
            });

            // show heatmap info
            $('#icon-legend').show();
            $('#heatmap-legend').show();
        }
    });

    // show/hide legend
    $( "#icon-legend" ).click(function() {
        // alert("icon-legend");
        $( "#heatmap-legend" ).toggle( "slow", function() {
            // Animation complete.
        });
    });

    // show/hide campaign image
    // https://stackoverflow.com/a/36317392
    // $('.trigger, .slider').click(function() {
    //     console.log('slider');
    //     $('.slider').toggleClass('close');
    // });

    // show/hide campaign stat
    // $('.stat-trigger, .stat-slider').click(function() {
    //     console.log('stat-slider');
    //     $('.stat-slider').toggleClass('stat-close');
    // });



    // center area coordinates: gps icon
    // alert('map-swipe-gps');
    $('#campaign-stat #map-swipe-gps').click(function() {
        // alert('map-swipe-gps click');
        var centerArea = localStorage.getItem( "center_area_coord" );
        var ctr = JSON.parse( centerArea );
        var val = ctr[0].latlong;
        var delm = ",";
        if (val != null) { // if center area
            var arrCenterCoord = val.split( delm );
            var centerPoint = new L.LatLng(arrCenterCoord[0], arrCenterCoord[1]);
            liveMap.panTo(centerPoint);
        }
        console.log('pan to center map');
    });
    $('#campaign-stat #map-swipe-gps').off().on('touchend', function() {
        // alert('map-swipe-gps touchend');
        var centerArea = localStorage.getItem( "center_area_coord" );
        var ctr = JSON.parse( centerArea );
        var val = ctr[0].latlong;
        var delm = ",";
        if (val != null) { // if center area
            var arrCenterCoord = val.split( delm );
            var centerPoint = new L.LatLng(arrCenterCoord[0], arrCenterCoord[1]);
            liveMap.panTo(centerPoint);
        }
        console.log('pan to center map');
    });
    // end of center area coordinates



    /*
    // old version -- touch swipe
    $(function() {      
        //Enable swiping...
        // http://labs.rampinteractive.co.uk/touchSwipe/demos/Swipe_status.html
        $("#map-swipe-area").swipe( {
            //Generic swipe handler for all directions
            swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
                // console.log("You swiped " + direction);
                if (direction == 'up') {
                    // $('.stat-slider').toggleClass('stat-close');

                    // open panel if hidden
                    var campaignStat = $('#campaign-stat');
                    if ($(campaignStat).hasClass("stat-first")) {
                        $(campaignStat).removeClass("stat-first");
                    }
                    if ($(campaignStat).hasClass("stat-close")) {
                        $(campaignStat).removeClass("stat-close");
                    }
                    console.log('open stat');
                }
                else if (direction == 'down') {
                    var campaignStat = $('#campaign-stat');
                    if (!($(campaignStat).hasClass("stat-close"))) {
                        if ($(campaignStat).hasClass("stat-first")) {
                            $(campaignStat).removeClass("stat-first");
                        }
                        $(campaignStat).addClass("stat-close");
                    }
                    console.log('close stat');
                }
            },
            //Default is 75px, set to 0 for demo so any distance triggers swipe
            threshold:0
        });

        // add swipe down to campaign stat, too
        $("#campaign-stat").swipe( {
            //Generic swipe handler for all directions
            swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
                var campaignStat = $('#campaign-stat');
                if (direction == 'down') {
                    if (!($(campaignStat).hasClass("stat-close"))) {
                        if ($(campaignStat).hasClass("stat-first")) {
                            $(campaignStat).removeClass("stat-first");
                        }
                        $(campaignStat).addClass("stat-close");
                    }
                    console.log('close stat from slider');
                }
                if (direction == 'up' && ($(campaignStat).hasClass("stat-first"))) {
                    $(campaignStat).removeClass("stat-first");
                }
            },
            //Default is 75px, set to 0 for demo so any distance triggers swipe
            threshold:0
        });
    });
    */

    // new version -- draggable (div position follows click/touch move, fixed width)
    // $("#campaign-stat").draggable();



    // apply filter
    var doFilterApply = document.querySelector("#filter-apply");
    doFilterApply.addEventListener('click', function(event){
        // alert('doFilterApply');
        showLoadingModal();



        // get data from filter: selected mode & campaign/sub in filter
        var fcId = document.querySelector( "select#select-filter-campaign" );
        var campaignId = fcId[fcId.selectedIndex].value || -1;
        var fscId = document.querySelector( "select#select-filter-subcampaign" );
        var subcampaignId = (fscId[fscId.selectedIndex])? fscId[fscId.selectedIndex].value : -1;
        localStorage.setItem("karta-campaign-id", campaignId);
        localStorage.setItem("karta-subcampaign-id", subcampaignId);

        var switchModes = document.querySelector( "ons-switch" );
        var switchModeSelected = switchModes.checked; // live/heatmap?

        var hl = document.querySelector("#heatmap-legend"); //legend
        var he = document.querySelector("#heatmap-empty"); //warning
        // end of get data from filter: selected mode & campaign/sub in filter



        // set campaign info; see ClientDashboardStats response
        var campaignNameObj = document.querySelector( "#select-filter-campaign" );
        var campaignName = campaignNameObj.options[campaignNameObj.selectedIndex].text;
        var contentCmp = document.querySelector( "#map-ctnt-campaign-info" ); 
        contentCmp.innerHTML = campaignName || "N/A";
        localStorage.setItem("karta-campaign-name", campaignName);

        var subcampaigns = localStorage.getItem( "subcampaigns" );
        var subcampaignsObj = JSON.parse(subcampaigns);
        if (subcampaignsObj.length > 0) {
            var subcampaignNameObj = document.querySelector( "#select-filter-subcampaign" );
            var subcampaignName = subcampaignNameObj.options[subcampaignNameObj.selectedIndex].text;
            var contentSubcmp = document.querySelector( "#map-ctnt-subcampaign-info" ); 
            contentSubcmp.innerHTML = subcampaignName || "N/A";
            localStorage.setItem("karta-subcampaign-name", subcampaignName);
        }
        // end of set campaign info; see ClientDashboardStats response



        var campaign_image_name = document.querySelector( "#campaign-name" );
        if (subcampaignsObj.length > 0) {
            campaign_image_name.innerHTML = campaignName + '<br />' + subcampaignName;
        }
        else {
            campaign_image_name.innerHTML = campaignName;
        }



        // set subcampaign image to filter AND panel
        var campaignImage = document.getElementById( "img-campaign" );
        var campaignImageSlider = document.getElementById( "campaign-image-img" );
        if (subcampaignId > 0) {
            var subcampaigns = localStorage.getItem( "subcampaigns" );
            var subcampaignsObj = JSON.parse(subcampaigns);
            var subcampaign = subcampaignsObj.find(function (obj) { return obj.id == subcampaignId; });
            campaignImage.src = "http://karta.id/titan/" + subcampaign.artwork_photo || "#";
            campaignImageSlider.src = "http://karta.id/titan/" + subcampaign.artwork_photo || "#";
        }
        else { // case: select sub, then select all
            // reset to image campaign
            var campaignUrl = localStorage.getItem( "karta-campaign-url" );
            campaignImage.src = campaignUrl || "";
            campaignImageSlider.src = campaignUrl || "";
        }
        // end of set subcampaign image to filter AND panel



        // change stat based on selected campaign; see setUpCampaign; add stat request
        var bcheck = localStorage.getItem( "campaign_list" );

        var campaigns = JSON.parse( bcheck );

        console.log( "num campaigns: ", campaigns.length );

        var camp = campaigns.find(function (obj) { return obj.campaign_id == campaignId; });

        setUpCampaign( camp );



        $.ajax({
            url: KartaConfig.apiGetDashboardStat(), // titanlight
            data: {
                campaign_id: campaignId,
                client_id: clientId,
                user_id: userId
            },
            method: "POST",
            type: "POST",
            dataType: 'json',
            headers: {"X-Authorization": apiAuthorization},
            success: function(detail, status){
                var info = detail.data.campaign_stats || {};

                // save to localStorage; copy from DashboardStatRequest
                localStorage.setItem("active_bikers", JSON.stringify(detail.data.active_bikers));
                localStorage.setItem("campaign_stats", JSON.stringify(info));
                localStorage.setItem("center_area_coord", JSON.stringify(detail.data.center_area_coord));
                localStorage.setItem("campaign", JSON.stringify(detail.data.campaign));
                localStorage.setItem("subcampaigns", JSON.stringify(detail.data.subcampaigns));
                window.mainActiveDrivers = detail.data.active_bikers;

                // set campaign stats info
                var stat1 = document.querySelector( "#km-stat" );
                var tamp =  parseFloat(info.total_km_driven) || 0;
                stat1.innerHTML = tamp.toLocaleString( "en-us" );

                var stat2 = document.querySelector( "#impression-stat" );
                var tamp2 =  parseFloat(info.flat_potential_view) || 0;
                stat2.innerHTML = tamp2.toLocaleString( "en-us" );

                var stat3 = document.querySelector( "#period-stat" );
                var tamp3 =  parseFloat(info.campaign_duration) || 0;
                stat3.innerHTML = tamp3 + " Month(s)";

                var stat4 = document.querySelector( "#driver-stat" );
                stat4.innerHTML = info.recruited_driver_text || "";

                // hideLoadingModal();
            },
            error: function(){
                alert('There was an error loading the data: get-dashboard-stats-info');
                // hideLoadingModal();
            }
        });
        // change stat based on selected campaign; see setUpCampaign; add stat request



        if (!switchModeSelected) { //live button clicked
            // alert('apply switch live');

            if( "geojsonHeatmap" in window ) { geojsonHeatmap.clearLayers() };
            
            // clear live map
            // add leaflet, clear live map & area
            if (lfmarkers) {
                lfmarkers.clearLayers();
            }
            if(geoArea) {
                liveMap.removeLayer(geoArea);
                console.log("remove geoarea");
            }
            // end of add leaflet, clear live map & area

            // copy ajax live
            $.ajax({
                url: KartaConfig.apiGetDashboardLive(), // titanlight
                data: {
                    campaign_id: campaignId,
                    client_id: clientId,
                    user_id: userId
                },
                method: "POST",
                dataType: 'json',
                headers: {"X-Authorization": apiAuthorization},
                // jsonp: 'jsoncallback',
                // timeout: 5000,
                success: function(data, status){
                    console.log('data: ', data);
                    // // save to localStorage; copy from DashboardLiveRequest
                    localStorage.setItem("biker_ids", JSON.stringify(data.data.biker_ids));
                    localStorage.setItem("biker_in_sse", JSON.stringify(data.data.biker_in_sse));
                    localStorage.setItem("campaign_area", JSON.stringify(data.data.campaign_area));
                    localStorage.setItem("live_bikers", JSON.stringify(data.data.live_bikers));
                    window.mainLiveDrivers = data.data.live_bikers;

                    setTimeout( function(){
                        drawCluster();
                        drawArea();
                        hideLoadingModal();
                    }, 2000 );
                },
                error: function(){
                    alert('There was an error loading the data: get-driver-live-route');
                    hideLoadingModal();
                }
            });

        } else { //heatmap button clicked
            console.log('clicked heatmap');

            // // add leaflet, clear live map & area
            // lfmarkers.clearLayers();
            // // map.removeLayer(geojsonArea);
            // for(i in map._layers) {
            //     if(map._layers[i]._path != undefined) {
            //         try {
            //             map.removeLayer(map._layers[i]);
            //         }
            //         catch(e) {
            //             console.log("problem with " + e + map._layers[i]);
            //         }
            //     }
            // }
            // // end of add leaflet, clear live map & area

            // // copy from doApply
            // heatmapHandler.request();
            // ClientHelper.blockUI();

            // hl.style.display="block";

            // copy from switch
            showLoadingModal();

            // ClientDashboardNavigation btnmode-heatmap
            // add leaflet, clear live map & area
            if (lfmarkers) {
                lfmarkers.clearLayers();
            }
            if(geoArea) {
                liveMap.removeLayer(geoArea);
                console.log("remove geoarea");
            }
            // end of add leaflet, clear live map & area

            // clear heatmap if any
            if (geojsonHeatmap) {
                geojsonHeatmap.clearLayers();
            }
            // end of clear heatmap if any

            var flagChoroplet = 'normal';
            var campaignId = localStorage.getItem("karta-campaign-id");
            var clientId = localStorage.getItem("karta-client-id");
            var userId = localStorage.getItem("karta-user-id");
            $.ajax({
                url: KartaConfig.apiGetClientDashboardHeatmap(),
                data: {
                    ajax_mode: "get-client-dashboard-heatmap",
                    user_id: userId,
                    client_id: clientId,
                    campaign_id: campaignId
                },
                method: "POST",
                dataType: 'json',
                headers: {"X-Authorization": apiAuthorization},
                success: function(data, status){
                    // alert('live&heatmap onchange');
                    console.log('data: ', data);
                    
                    choroplethFeatureCollection = []; // clear
                    cfc = []; // clear

                    // heatmap must be >=14 days from start campaign date
                    var min_date = data.data.min_date;
                    var biweek_date = data.data.biweek_date;
                    localStorage.setItem("biweek_date", biweek_date);

                    let today = moment(new Date());
                    var biweek_date = moment(biweek_date, "YYYY-MM-DD");
                    if ( moment(today).isAfter(moment( biweek_date )) ) { // if today > campaign end date
                        // display heatmap
                        choropleths = data.data.choropleth_areas || undefined;
                        if(!choropleths) {
                            // console.log("cek c");
                            choropleths= data.data.choropleth_area_daterange || undefined;
                            flagChoroplet = "range";
                            if(!choropleths)
                            {
                                console.log("cek d");
                            // KartaBase.finishUI();
                                return false;
                            }

                        }
                        if(flagChoroplet === "normal")
                        {
                            var a = data.data.choropleth_area_daily;
                            var lengthDaily = Object.keys(a).length;
                            // console.log("data daily",Object.keys(a).length);
                            localStorage.setItem("min_date_daily",Object.keys(a)[0]);
                            localStorage.setItem("max_date_daily",Object.keys(a)[lengthDaily- 1]);
                            // console.log("Object value", Object.keys(a)[0], lengthDaily);

                        }
                        console.log("heatmap data ", data);
                        
                        localStorage.removeItem( "choropleths_"+campaignId);
                        localStorage.setItem( "choropleths_"+campaignId, JSON.stringify( data ) );
                        
                        console.log("locals item",choropleths);
                        
                        var i, iLen = choropleths.length;
                        // var heatmap = KartaBase.getDashboardHeatmap();
                        for( i=0;i<iLen;++i ) {
                            console.log('choropleths[', i, ']: ', choropleths[i]);
                            
                            // totalWeight = choropleths[i].total_weight;
                            // totalKmdistance = choropleths[i].total_km_campaign;
                            var obj = self._createChoroplethArea( choropleths[i],flagChoroplet);

                            // heatmap.appendChild( obj );

                            //var tamp = self._createLfLayer( map, json[i] ); console.log('tamp[', i, ']: ', tamp);
                            // areaLayers.push( tamp );
                        }
                        window.areaLayers = areaLayers;

                        setTimeout( function(){
                            console.log("visualizeIt");

                            console.log('choroplethFeatureCollection before _cfc: ', choroplethFeatureCollection);
                            self._cfc();
                            self._drawChoropleth();
                            
                            hideLoadingModal();
                        }, 3000 );
                    }
                    else {
                        // display empty heatmap
                        var empty = document.getElementById("empty-heatmap-content");
                        empty.style.display = "block";
                    }
                },
                error: function(){
                    alert('There was an error loading the data.: get-client-dashboard-heatmap');
                    hideLoadingModal();
                }
            });

            // show heatmap info
            $('#icon-legend').show();

        }

        $("#filter-select").slideReveal("hide"); // HIDE FILTER PANEL
    });
}







function requestDashboardStat() {
    // alert('requestDashboardStat');
    var apiAuthorization = KartaConfig.apiAuthorization;

    var campaignId = localStorage.getItem("karta-campaign-id");
    var clientId = localStorage.getItem("karta-client-id");
    var userId = localStorage.getItem("karta-user-id");

    $.ajax({
        url: KartaConfig.apiGetDashboardStat(), // titanlight
        data: {
            campaign_id: campaignId,
            client_id: clientId,
            user_id: userId
        },
        method: "POST",
        type: "POST",
        dataType: 'json',
        headers: {"X-Authorization": apiAuthorization},
        // xhr: function()
        // {
        //     var xhr = new window.XMLHttpRequest();
        //     //Upload progress
        //     xhr.upload.addEventListener("progress", function(evt){
        //         if (evt.lengthComputable) {
        //             var percentComplete = evt.loaded / evt.total;
        //             //Do something with upload progress
        //             console.log(percentComplete);
        //         }
        //     }, false);
        //     //Download progress
        //     xhr.addEventListener("progress", function(evt){
        //         if (evt.lengthComputable) {
        //             var percentComplete = evt.loaded / evt.total;
        //             //Do something with download progress
        //             console.log(percentComplete);
        //         }
        //     }, false);
        //     return xhr;
        // },
        success: function(detail, status){
            var info = detail.data.campaign_stats || {};

            // save to localStorage; copy from DashboardStatRequest
            localStorage.setItem("active_bikers", JSON.stringify(detail.data.active_bikers));
            localStorage.setItem("campaign_stats", JSON.stringify(info));
            localStorage.setItem("center_area_coord", JSON.stringify(detail.data.center_area_coord));
            localStorage.setItem("campaign", JSON.stringify(detail.data.campaign));
            localStorage.setItem("subcampaigns", JSON.stringify(detail.data.subcampaigns));
            window.mainActiveDrivers = detail.data.active_bikers;

            // set info to map&driver
            var campaignName = localStorage.getItem( "karta-campaign-name" );
            var campaignUrl = localStorage.getItem( "karta-campaign-url" );
            var campaignEmail = localStorage.getItem( "karta-user-email" );
            if (campaignEmail == 'demo@karta.asia') { // demokarta
                campaignName = "Karta Demo";
                campaignUrl = "images/bensin.jpg";
                localStorage.setItem("karta-campaign-name", campaignName);
                localStorage.setItem("karta-campaign-url", campaignUrl);

                // HIDE FILTER FTW
                var iconSearch = document.getElementById( "icon-search" );
                iconSearch.style.display = "none";
                var driverIconSearch = document.getElementById( "driver-icon-search" );
                driverIconSearch.style.display = "none";
            }

            var contentCmp = document.querySelector( "#map-ctnt-campaign-info" );
            contentCmp.innerHTML = campaignName || "N/A";
            var campaignImg = document.querySelector("#img-campaign");
            campaignImg.src = campaignUrl || "";

            var subcampaignObj = localStorage.getItem("subcampaigns");
            var subcampaigns = JSON.parse(subcampaignObj);
            if (subcampaigns.length > 0) { // existed subcampaign
                var contentSubCmp = document.querySelector( "#map-ctnt-subcampaign-info" );
                contentSubCmp.innerHTML = "All subcampaigns";
            }
            else {
                var divInfoSubCmp = document.querySelector( "#div-map-info-subcampaign" );
                divInfoSubCmp.style.display = "none";
                var divFilterSubCmp = document.querySelector( "#div-map-filter-subcampaign" );
                divFilterSubCmp.style.display = "none";
            }

            // set campaign image&name
            // var campaign_image_ctnt = info.campaign_photo; // if demokarta
            var campaign_image_ctnt = localStorage.getItem( "karta-campaign-url" );
            // var campaign_image = document.querySelector( "#campaign-image" );
            // campaign_image.innerHTML = '<div class="campaign-swipe-image" class=""></div>'
            //     + '<img src="'+campaign_image_ctnt+'" alt="Spotify" height="291" width="233" />'
            //     + '<p><span>' + campaignName + '</span></p>';
            var campaign_image_img = document.querySelector( "#campaign-image-img" );
            campaign_image_img.src = campaign_image_ctnt || "#";
            var campaign_image_name = document.querySelector( "#campaign-name" );
            campaign_image_name.innerHTML = campaignName || "Campaign";

            // set today & count remaining days
            // var descr = moment( updated, "YYYY-MM-DD HH:mm:ss" ).format( "DD MMM YYYY HH:mm" ) || "N/A";
            let today = moment(new Date()).format( "DD MMM YYYY " );
            var end_date = moment('2018-03-25', "YYYY-MM-DD").format( "DD MMM YYYY" ); // example astra end date
            // var end_date = moment(detail.data.campaign.end_date, "YYYY-MM-DD").format( "DD MMM YYYY" );
            var days = '12 days';
            if ( moment(today).isAfter(moment( end_date )) ) { // if today > campaign end date
                days = 'Finished';
            }
            else {
                days = moment(end_date).diff(moment(today), "days") + ' Days Left';
            }
            var campaign_stat_today = document.querySelector( "#campaign-stat-today" );
            campaign_stat_today.innerHTML = 'Today, ' + today;
            var campaign_stat_remaining = document.querySelector( "#campaign-stat-remaining" );
            campaign_stat_remaining.innerHTML = days;

            // set campaign stats info
            var stat1 = document.querySelector( "#km-stat" );
            var tamp =  parseFloat(info.total_km_driven) || 0;
            stat1.innerHTML = tamp.toLocaleString( "en-us" );

            var stat2 = document.querySelector( "#impression-stat" );
            var tamp2 =  parseFloat(info.flat_potential_view) || 0;
            stat2.innerHTML = tamp2.toLocaleString( "en-us" );

            var stat3 = document.querySelector( "#period-stat" );
            var tamp3 =  parseFloat(info.campaign_duration) || 0;
            stat3.innerHTML = tamp3 + " Month(s)";

            var stat4 = document.querySelector( "#driver-stat" );
            stat4.innerHTML = info.recruited_driver_text || "";

            // set filter campaign/sub
            setFilterCampaign();
            // set panel
            setupMapFilterPanelListener();

            // $("#img-campaign").click(hideAndShowCampaignImage); // $('#card-map div:not(.col-search)')
            $('#card-map').click(hideAndShowCampaignImage);
            $("#campaign-image-overlay").click(hideAndShowCampaignImage);
            $(".map-slider-close-btn").click(hideAndShowCampaignImage);

            setTimeout( function(){
                hideLoadingModal();
            }, 800 );
        },
        error: function(){
            alert('There was an error loading the data: get-dashboard-stats-info');
            hideLoadingModal();
            showRetryAlert();
        },
        timeout: 48000 // sets timeout to 12 seconds
    });
}



function requestDashboardLive() {
    // alert('requestDashboardLive');
    var apiAuthorization = KartaConfig.apiAuthorization;

    var campaignId = localStorage.getItem("karta-campaign-id");
    var clientId = localStorage.getItem("karta-client-id");
    var userId = localStorage.getItem("karta-user-id");

    $.ajax({
        url: KartaConfig.apiGetDashboardLive(), // titanlight
        data: {
            campaign_id: campaignId,
            client_id: clientId,
            user_id: userId
        },
        method: "POST",
        dataType: 'json',
        headers: {"X-Authorization": apiAuthorization},
        // jsonp: 'jsoncallback',
        // timeout: 5000,
        success: function(data, status){
            console.log('data: ', data);
            // // save to localStorage; copy from DashboardLiveRequest
            localStorage.setItem("biker_ids", JSON.stringify(data.data.biker_ids));
            localStorage.setItem("biker_in_sse", JSON.stringify(data.data.biker_in_sse));
            localStorage.setItem("campaign_area", JSON.stringify(data.data.campaign_area));
            localStorage.setItem("live_bikers", JSON.stringify(data.data.live_bikers));
            window.mainLiveDrivers = data.data.live_bikers;

            setTimeout( function(){
                drawCluster();
                drawArea();
                // hideLoadingModal();
            }, 2000 );
        },
        error: function(){
            alert('There was an error loading the data: get-driver-live-route');
        }
    });
}







// reset filter
var doFilterReset = document.querySelector("#filter-reset");
doFilterReset.addEventListener('click', function(event){
    // get previously selected campaign
    var clientId = localStorage.getItem("karta-client-id");
    var campaignId = localStorage.getItem("karta-campaign-id");
    var subcampaignId = localStorage.getItem("karta-subcampaign-id") || -1;
    var subcampaigns = localStorage.getItem( "subcampaigns" );
    var subcampaignsObj = JSON.parse(subcampaigns);
    var userId = localStorage.getItem("karta-user-id");

    // clear subcampaign
    if (subcampaignsObj.length > 0) {
        localStorage.setItem("karta-subcampaign-id", -1);
        localStorage.setItem("karta-subcampaign-name", "All subcampaigns");

        // chg selected to all subs
        var select = document.getElementById("select-filter-subcampaign");
        select.value = -1;
    }
    // end of clear subcampaign

    // chg image to cmp image & set sub all camp; copy from filter apply
    var campaignImage = document.getElementById( "img-campaign" );
    var campaignImageSlider = document.getElementById( "campaign-image-img" );
    var campaignUrl = localStorage.getItem( "karta-campaign-url" );
    campaignImage.src = campaignUrl || "";
    campaignImageSlider.src = campaignUrl || "";

    var campaignName = localStorage.getItem( "karta-campaign-name" );
    var contentCmp = document.querySelector( "#map-ctnt-campaign-info" );
    var campaignNameSlider = document.getElementById( "campaign-name" );
    contentCmp.innerHTML = campaignName || "N/A";
    campaignNameSlider.innerHTML = campaignName || "N/A";

    if (subcampaignsObj.length > 0) { // existed subcampaign
        var contentSubCmp = document.querySelector( "#map-ctnt-subcampaign-info" );
        contentSubCmp.innerHTML = "All subcampaigns";
    }
    // chg image to cmp image & set sub all camp; copy from filter apply

    var switchModes = document.querySelector( "ons-switch" );
    var switchModeSelected = switchModes.checked; // live/heatmap?

    if (!switchModeSelected) { //live button clicked
        $("#filter-select").slideReveal("hide"); // HIDE FILTER PANEL

        if( "geojsonHeatmap" in window ) { geojsonHeatmap.clearLayers() };

        // clear live map
        // add leaflet, clear live map & area
        if (lfmarkers) {
            lfmarkers.clearLayers();
        }
        if(geoArea) {
            liveMap.removeLayer(geoArea);
            console.log("remove geoarea");
        }
        // end of add leaflet, clear live map & area

        // copy ajax live
        var apiAuthorization = KartaConfig.apiAuthorization;
        $.ajax({
            url: KartaConfig.apiGetDashboardLive(), // titanlight
            data: {
                campaign_id: campaignId,
                client_id: clientId,
                user_id: userId
            },
            method: "POST",
            dataType: 'json',
            headers: {"X-Authorization": apiAuthorization},
            // jsonp: 'jsoncallback',
            // timeout: 5000,
            success: function(data, status){
                console.log('data: ', data);
                // // save to localStorage; copy from DashboardLiveRequest
                localStorage.setItem("biker_ids", JSON.stringify(data.data.biker_ids));
                localStorage.setItem("biker_in_sse", JSON.stringify(data.data.biker_in_sse));
                localStorage.setItem("campaign_area", JSON.stringify(data.data.campaign_area));
                localStorage.setItem("live_bikers", JSON.stringify(data.data.live_bikers));
                window.mainLiveDrivers = data.data.live_bikers;

                setTimeout( function(){
                    drawCluster();
                    drawArea();
                }, 2000 );
            },
            error: function(){
                alert('There was an error loading the data: get-driver-live-route');
            }
        });

    } else { //heatmap button clicked
        console.log('clicked heatmap');

        // add leaflet, clear live map & area
        lfmarkers.clearLayers();
        // map.removeLayer(geojsonArea);
        for(i in map._layers) {
            if(map._layers[i]._path != undefined) {
                try {
                    map.removeLayer(map._layers[i]);
                }
                catch(e) {
                    console.log("problem with " + e + map._layers[i]);
                }
            }
        }
        // end of add leaflet, clear live map & area

        // copy from doApply
        heatmapHandler.request();
        ClientHelper.blockUI();

        hl.style.display="block";
    }
});



//  set filter campaign/sub
function setFilterCampaign() {
    var campaigns = localStorage.getItem( "campaign_list" );
    var campaignsObj = JSON.parse(campaigns); console.log('campaigns: ', campaignsObj);
    var subcampaigns = localStorage.getItem( "subcampaigns" );
    var subcampaignsObj = JSON.parse(subcampaigns); console.log('subcampaigns: ', subcampaignsObj);

    // CLEAR; SHOW?HIDE THE SUBS IF EXISTS
    var select = document.getElementById("select-filter-campaign");
    select.innerHTML = ""; // clear
    for(var i = 0; i < campaignsObj.length; i++) {
        var option = document.createElement("option");
        option.text = campaignsObj[i].campaign_name;
        option.value = campaignsObj[i].campaign_id;
        select.appendChild(option);
    }
    var select = document.getElementById("select-filter-subcampaign");
    select.innerHTML = ""; // clear
    var option = document.createElement("option");
    option.text = "All subcampaigns";
    option.value = -1;
    select.appendChild(option);
    for(var i = 0; i < subcampaignsObj.length; i++) {
        var option = document.createElement("option");
        option.text = subcampaignsObj[i].subcampaign_name;
        option.value = subcampaignsObj[i].id;
        select.appendChild(option);
    }

    // copy to driver filter
    var select = document.getElementById("select-driver-filter-campaign");
    select.innerHTML = ""; // clear
    for(var i = 0; i < campaignsObj.length; i++) {
        var option = document.createElement("option");
        option.text = campaignsObj[i].campaign_name;
        option.value = campaignsObj[i].campaign_id;
        select.appendChild(option);
    }
    var select = document.getElementById("select-driver-filter-subcampaign");
    select.innerHTML = ""; // clear
    var option = document.createElement("option");
    option.text = "All subcampaigns";
    option.value = -1;
    select.appendChild(option);
    for(var i = 0; i < subcampaignsObj.length; i++) {
        var option = document.createElement("option");
        option.text = subcampaignsObj[i].subcampaign_name;
        option.value = subcampaignsObj[i].id;
        select.appendChild(option);
    }
}

// leaflet get popup driver info
function getLeafInfo(i) {
    var self = this;

    var driver = mainActiveDrivers[ i ];
    var latest_route = driver.latest_route || '';
    var biker_name = driver.name || "N/A";
    var bikerId = driver.biker_id || 0;
    var startDate = driver.start_register || '';
    var finishDate = driver.expiry_register || '';
    // var updated = liveUpdates[bikerId] || driver.last_updated;
    var updated = driver.last_updated || '';
    // var descr = moment( updated, "YYYY-MM-DD HH:mm:ss" ).format( "DD MMM YYYY HH:mm" ) || "N/A";
    var kmdist = driver.flat_route_km || 0;
    var duration = driver.biker_committed_days || 0;
    var img = KartaConfig.assetsUrl + driver.biker_photo;

    var campaignEmail = localStorage.getItem( "karta-user-email" );
    if (campaignEmail == 'demo@karta.asia') { // demokarta
        img = "images/profile_blue_karta.png";
    }

    // 18 sep change biker name
    var bikname = '';
    var arr_name = biker_name.split(" ");
    if (arr_name.length > 0) {
        for (var j=0; j<arr_name.length; j++) {
            var sub = arr_name[j].substring(0, 1);
            bikname += sub + ".";
        }
    }
    // end of 18 sep change biker name

    var contentStringImg = '<div style="float:left;width:60px;height:60px;">'
        +'<img src="'+img+'" width="60" height="60" />'
        +'</div><div style="float:right; padding: 10px;">'
        +'Nama: '+bikname+'<br/>'
        // +'Start: '+moment(startDate, "YYYY-MM-DD").format( "DD MMM YYYY" )+'<br/>'
        // +'End: '+moment(finishDate, "YYYY-MM-DD").format( "DD MMM YYYY" )+'<br/>'
        +'Start: '+startDate+'<br/>'
        +'End: '+finishDate+'<br/>'
        +'Km Final: '+kmdist+' km<br/>'
        // +'<button type="button" id="btn_showmap_'+i+'" class="btn btn-primary" onclick="liveHandler.displayAnimate('+i+');"><span class="glyphicon glyphicon-play"></span>&nbsp;Playback</button></div>'
        ;
    return contentStringImg;
    // +'position: '+latest_route+' km<br/>'
}
// end of leaflet get popup driver info

// function to draw cluster
function drawCluster() {
	// alert('drawCluster'); console.log('mainActiveDrivers: ', mainActiveDrivers);

	var locations = [];
	// var drawActiveDrivers = mainActiveDrivers; // JSON.parse(mainActiveDrivers);

    // add subcampaign filter
    var subcampaign = localStorage.getItem( "karta-subcampaign-id" ) || -1;
    console.log('subcampaign: ', subcampaign);
    var drawActiveDrivers = []; // draw active drivers, filtered by subc selected
    if (subcampaign > 0) {
        console.log('1. create markerCluster w sub ', subcampaign);
        // https://stackoverflow.com/a/13964186
        var result = mainActiveDrivers.filter(function( obj ) {
            return obj.subcampaign_id == subcampaign;
        });
        drawActiveDrivers = result;
    }
    else {
        console.log('2. create markerCluster');
        drawActiveDrivers = mainActiveDrivers;
    }
    console.log('drawActiveDrivers: ', drawActiveDrivers);
    // end of add subcampaign filter

	for (var i=0; i<drawActiveDrivers.length; i++) {
        var location = [];
        var latestRoute = drawActiveDrivers[i].latest_route;
        // var arrLatestRoute = latestRoute.split(",");
        var arrLatestRoute = [];
        if (latestRoute != null) {
        	arrLatestRoute = latestRoute.split(",");
        }
        // console.log('arrLatestRoute: ', arrLatestRoute);

        // if marker not exists in hist / queue / outside area then place in center area
        if (arrLatestRoute == '' || arrLatestRoute == '0' || arrLatestRoute == 0 || arrLatestRoute == []) {
            var centerArea = localStorage.getItem( "getLatLong" ); // console.log('centerArea: ', centerArea);
            // var ctr = JSON.parse( centerArea );
            var val = centerArea;
            var delm = ",";
            if (val != null) { // if center area
                arrLatestRoute = val.split( delm );
            }
        }

        location.push(drawActiveDrivers[i].biker_name);
        location.push(arrLatestRoute[0]);
        location.push(arrLatestRoute[1]);

        locations.push(location);
    }
    console.log('leaflet cluster, locations: ', locations);

    // create custom icon
    var leafIcon = L.icon({
        iconUrl: 'https://karta.id/titan/assets/components/client-ui/images/pin-karta.png',
        iconSize: [24, 30], // size of the icon
        iconAnchor: [10, 45], // point of the icon which will correspond to marker's location
        popupAnchor: [3, -48], // point from which the popup should open relative to the iconAnchor
    });

    // specify popup options 
    var customOptions =
    {
        'maxWidth': '500',
        'height': '500',
        'className' : 'custom'
    };
    var customPopup = '';

    var markerClusters = L.markerClusterGroup();
    for (var i = 0; i < locations.length; i++) {
        // create popup contents
        customPopup = getLeafInfo(i);

        if (locations[i][1] && locations[i][2]) {
            // var marker = new L.marker([locations[i][1],locations[i][2]], {icon: leafIcon})
            var marker = new L.marker([locations[i][1],locations[i][2]])
                // .bindPopup(locations[i][0])
                .bindPopup(customPopup, customOptions);
                // .addTo(map);

            // add cluster
            markerClusters.addLayer(marker);
        }
    }
    window.lfmarkers = markerClusters; // add to window; to modify/clear when needed
    console.log('liveMap: ', liveMap);
    liveMap.addLayer(markerClusters);
}

function drawArea() {
    console.log('drawArea');
    // NEW add area, clear first; center area
    console.log("GEOAREA",geoArea);
    if(geoArea) {
        liveMap.removeLayer(geoArea);
        console.log("remove geoarea");
    }
    
    // var geojsonArea = JSON.parse(localStorage.getItem('campaign-area'));
    var areaUrl = localStorage.getItem('campaign_area') || '';
    areaUrl = JSON.parse(areaUrl);
    $.getJSON( areaUrl, {
        
    })
    .done(function( data ) {
        geoArea = L.geoJSON(data).addTo(liveMap);
        console.log("input geoarea",geoArea);
        
        localStorage.setItem('campaign-area-json', JSON.stringify(data));
    });
    //end of add area, clear first; center area

    // center area coordinates
    var centerArea = localStorage.getItem( "getLatLong" );
    // var ctr = JSON.parse( centerArea );
    // var val = ctr.latlong;
    var delm = ",";
    if (centerArea != null) { // if center area
        var arrCenterCoord = centerArea.split( delm );
        var centerPoint = new L.LatLng(arrCenterCoord[0], arrCenterCoord[1]);
        liveMap.panTo(centerPoint);
    }
    // end of center area coordinates
}

// heatmap function
var areaLayers = [];
var newArealyer = [];
var totalWeight = 0;
var areaLayer;
var map;
// var newArealyer;
var mcenterLat,mcenterLng=0;
var newkmdistance, totalKmdistance, totalKmCampaign , minDist, maxDist , kmDistanceRange, count= 0;

// lf var
var choroplethFeatureCollection = [];
var cfc;
var choroplethCount = 0;
var geoHeatmap;

// function _createLayer( map, json ){
function _createChoroplethArea(json, mode) {
    var self = this;

    choroplethCount++;
    totalKmdistance = json.total_km;
    totalKmCampaign = json.total_km_campaign;
    maxDist = json.max_km;

    // using json -- 1 jul
    // var choroplethUrl = $(json).attr('s3-url');
    var choroplethUrl = json.s3_url;
    console.log('choroplethUrl ' + choroplethUrl);
    var choroplethJson;
    $.getJSON( choroplethUrl, {
        
    })
    .done(function( data ) {
        console.log('$.getJSON done ', data);
        // // choropleth.push(data);
        choroplethJson = data; // json object

        // commented 25 sep; changed to weight&kmdist
        var weight = choroplethJson.features[0].properties.weight; console.log('weight: ', weight);
        var kmdistance = choroplethJson.features[0].properties.kmdistance; console.log('kmdistance: ', kmdistance);
        // commented 25 sep; changed to weight&kmdist

        // var weight = $(json).attr('weight');
        // var kmdistance = $(json).attr('kmdistance');

        var numKm = kmdistance;
        var color = _compute( kmdistance, "weight" ); // #REV 29 nov
        console.log("color _compute kmdistance: ", color);
        choroplethJson.features[0].properties.color = color;
        console.log('choroplethJson data: ', data);

        choroplethFeatureCollection.push(choroplethJson);

    });
}

function _compute( num, mode ){
    console.log('_compute, num, mode: ', num, ',', mode);
    var thisnum = num;
    var flag = localStorage.getItem("flagChoroplet");
    console.log("flag choropl", flag);
    console.log("totalKmdistance number ",totalKmdistance );
    console.log("totalKmCampaign number ",totalKmCampaign );
    console.log("max ",maxDist ); //console.log("min ",minDist );
    var percentage,finalPercentage,lgndLmt,l1,l2,l3,l4,l5,areaVal,maxLgnd = 0;
    
    finalPercentage = parseFloat((totalKmCampaign/totalKmdistance));
    maxLgnd = parseFloat(maxDist*finalPercentage);
    lgndLmt = (maxLgnd/5);
    if(totalKmdistance===-1)
    {
        lgndLmt = 0;
    }
    l1 = lgndLmt;
    l2 = l1+lgndLmt;
    l3 = l2 + lgndLmt;
    l4 = l3 + lgndLmt;
    l5 = l4 + lgndLmt;
    console.log("lgndLmt ",lgndLmt, ",",l2,",",l3,",",l4,",",l5 );
    // console.log("kmdistance2 ",newkmdistance );
    // percentage = parseFloat((num/totalWeight)*100);
    areaVal = parseFloat(thisnum * finalPercentage ); console.log('areaVal: ', areaVal);



    /************************************ NEW 2 add to legend ************************************/
    var val1, val2,val3,val4,val5,  counter =0;
    var txt1, txt2,txt3,txt4,txt5 = "";
    var valAdd = "0";
    var  txtLgnd = parseFloat(maxDist/5);
    val1 = txtLgnd;
    val2 = val1+txtLgnd;
    val3 = val2 + val1;
    val4 = val3 + val1;
    val5 = val4 + val1;
    txt1 = val1.toString().charAt(0);
    txt2 = val2.toString().charAt(0);
    txt3 = val3.toString().charAt(0);
    txt4 = val4.toString().charAt(0);
    txt5 = val5.toString().charAt(0);
    var lenChar = Math.floor(maxDist).toString().length;
    var minLenChar = Math.floor(txtLgnd).toString().length;
    // console.log("txt 1:",txt1,txt2,txt3,txt4,txt5, valAdd);
    // console.log("val ",val1,val2,val3,val4,val5, lenChar,maxDist);

    if(Math.floor(val2).toString().length > minLenChar)
    {
        txt2 = txt2.toString()+val2.toString().charAt(1);
        // console.log("flag2");
    }

    if(Math.floor(val3).toString().length > minLenChar)
    {
        txt3 = txt3.toString()+val3.toString().charAt(1);
        // console.log("flag3");
    }

    if(Math.floor(val4).toString().length > minLenChar)
    {
        txt4 = txt4.toString()+val4.toString().charAt(1);
        // console.log("flag4");
    }
    if(Math.floor(val5).toString().length > minLenChar)
    {
        txt5 = txt5.toString()+val5.toString().charAt(1);
        // console.log("flag5");
    }
    
    // console.log("txt 2:",txt1,txt2,txt3,txt4,txt5, valAdd);
    do{
        txt1 += valAdd;
        txt2 += valAdd;
        txt3 += valAdd;
        txt4 += valAdd;
        txt5 += valAdd;
        counter++
    }
    while
        (counter<minLenChar-1);
    // console.log("txts ",txt1,txt2,txt3,txt4,txt5, valAdd);
    if(parseFloat(txt1) > parseFloat(txt5))
    {
        txt1 = txt1.substring(0, 2);
    }
    console.log("txt ",txt1,txt2,txt3,txt4,txt5, valAdd);
    var lgn1 = document.querySelector( "#lg1" );
    lgn1.innerHTML = "< " + txt1;
    var lgn2 = document.querySelector( "#lg2" );
    lgn2.innerHTML = txt1 + " - < " + txt2;
    var lgn3 = document.querySelector( "#lg3" );
    lgn3.innerHTML = txt2 + " - < " +txt4;
    var lgn4 = document.querySelector( "#lg4" );
    lgn4.innerHTML = txt4 + " - < " + txt5;
    var lgn5 = document.querySelector( "#lg5" );
    lgn5.innerHTML = "> "+txt5;
    /************************************ END OF NEW 2 add to legend ************************************/



    percentage = parseFloat((thisnum/maxDist)*100);
    if( mode == "weight" ){
        var weight = thisnum;
        // console.log("percentage ",Math.floor(percentage));
        // if(num <= 0) return 10;
        // if(num < 25.00 && num > 0 ) return 5;
        //   else if(25.00 <= num &&  num < 45.00) return 6;
        //   else if(45.00 <= num &&  num < 65.00) return 7;
        //   else if(65.00 <= num &&  num <= 85.00) return 8;
        //   else if(num > 85.00) return 9;

        // return 10;
        if(areaVal <= 0) return 10;
        if(areaVal < l1 && areaVal > 0 ) return 5;
            else if(l1 <= areaVal &&  areaVal < l2) return 6;
            else if(l2 <= areaVal &&  areaVal < l4) return 7;
            else if(l4 <= areaVal &&  areaVal <= l5) return 8;
            else if(areaVal > l5) return 9;

        // return 10;
    }
}

function _cfc() {
    // alert('_cfc');
    var self = this;

    console.log("_cfc, choroplethFeatureCollection: ", choroplethFeatureCollection);
    
    // merge collection
    console.log("choroplethFeatureCollection.length: ", choroplethFeatureCollection.length);
    cfc = choroplethFeatureCollection[0];
    if (choroplethFeatureCollection.length > 1) {
        for( var i=1; i<choroplethFeatureCollection.length; i++ ) {
            var f = choroplethFeatureCollection[i].features;
            cfc.features.push(f[0]);
        }
    }
    console.log("cfc: ", cfc);
    window.choroplethFeatureCollection = choroplethFeatureCollection;
    return cfc;
    // end of merge collection
}

function _drawChoropleth() {
    var self = this;

    console.log("_drawChoropleth, geojsonHeatmap: ", geojsonHeatmap);

    // insert feature collection to map (draw choropleth), add tooltip
    if (geojsonHeatmap) {
        // alert('clear layers');
        geojsonHeatmap.clearLayers();
        map.removeLayer(geojsonHeatmap);
        map.removeLayer(geoHeatmap);
        geojsonHeatmap = null;
        geoHeatmap = null;

        // for(i in map._layers) {
        //     if(map._layers[i]._path != undefined) {
        //         try {
        //             map.removeLayer(map._layers[i]);
        //         }
        //         catch(e) {
        //             console.log("problem with " + e + map._layers[i]);
        //         }
        //     }
        // }
    }

    var geojsonHeatmap = L.geoJson(choroplethFeatureCollection, {
        style: addStyle,
        onEachFeature: onEachFeature
    })
    .bindTooltip(function (layer) {
        return layer.feature.properties.name + '<br />Km: ' + layer.feature.properties.kmdistance;
    });

    // var heatMap = document.querySelector('#live-map-container');
    // console.log('heatMap: ', heatMap);
    // // if(! (window.geojsonHeatmap)) {
    //     geojsonHeatmap.addTo(heatMap);
    // // }
    geojsonHeatmap.addTo(liveMap);
    
    geoHeatmap = geojsonHeatmap;
    if( ! ("geojsonHeatmap" in window) ) window.geojsonHeatmap = geojsonHeatmap; // window.geojsonHeatmap = geojsonHeatmap;
    // end of insert feature collection to map (draw choropleth), add tooltip
}

var colors = [
    "rgba(206,214,227, 0.0)",
    "rgba(128,148,179, 0.8)",
    "rgba(79,109,156, 0.8)",
    "rgba(29,43,67, 0.8)",
    "rgba(22,29,43, 0.8)",

    "rgba(255,124,124, 0.3)",
    "rgba(242,80,80, 0.66)",
    "rgba(226,39,39, 0.78)",          
    "rgba(183,16,16, 0.8)",
    "rgba(145,0,0, 0.8)",
    "rgba(226, 224, 224, 0.5)"
];

function addStyle(feature) {
    var self = this;
    // console.log('addStyle ', feature, hcolors);

    // var styleColor = getColor(feature.properties.color);
    var styleColor = colors[feature.properties.color];
    // var styleColor = hcolors[feature.properties.color];

    // var styleFillOpacity = Math.random();
    return {
        weight: 2,
        opacity: 1,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.3,
        // fillOpacity: styleFillOpacity,
        fillColor: styleColor
    };
}

function onEachFeature(feature, layer) {
    var self = this;

    // console.log('feature: ', feature);
    // console.log('layer: ', layer);
    // layer.on({
    //     mouseover: self.highlightFeature,
    //     mouseout: self.resetHighlight,
    //     click: self.zoomToFeature
    // });

    // https://stackoverflow.com/a/27749229
    layer.on('mouseover', function () {
        this.setStyle({
            'fillColor': '#0000ff'
        });
        var layer = this;

        // ?TODO MOUSEOVER FUNCTION CHORO

        // this.setStyle({
        //     weight: 5,
        //     color: '#666',
        //     dashArray: '',
        //     fillOpacity: 0.1
        // });

        // if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        //     this.bringToFront();
        // }
    });
    layer.on('mouseout', function () {
        // this.setStyle({
        //     'fillColor': '#ff0000'
        // });

        // console.log('mouseout, this.target: ', this.target, this);
        geojsonHeatmap.resetStyle(this);
    });
    layer.on('click', function () {
        // Let's say you've got a property called url in your geojsonfeature:
        // window.location = feature.properties.url;
        liveMap.fitBounds(this._bounds);
    });
}

function _clearLayer() { // copy dari kdh.html
    var self = this;
    console.log("areaLayers",areaLayers);
    //areaLayer.revertStyles();
    var i, iLen = areaLayers.length;
    for( i=0;i<iLen;++i ) {
        areaLayers[i].setMap( null );
    }
    areaLayers = [];
}

// ?TODO ADD FLAG LOGIN

// https://nnattawat.github.io/slideReveal/ Overlay Panel
function setupMapFilterPanelListener() {
    $("#filter-select").slideReveal({
        trigger: $("#icon-search"),
        position: "right",
        push: false,
        overlay: true
    });
}

// toggle image campaign from click top left img, inside filter card
function hideAndShowCampaignImage(){
    var details = $('#campaign-image');
    var courtain = $('#campaign-image-overlay');
    
    if(courtain.hasClass('inactive')){
        courtain.removeClass('inactive');
        courtain.addClass('active');
        details.removeClass('close');
    }
    else{
        courtain.removeClass('active');
        courtain.addClass('inactive');
        details.addClass('close');
    }
}

function showLoadingModal() {
    var modal = document.getElementById("loading-modal"); console.log(modal);
    modal.show();
    console.log("showLoadingModal");

    setTimeout( function(){
        hideLoadingModal();
    }, 12000 );
}

function hideLoadingModal() {
    var modal = document.getElementById("loading-modal");
    modal.hide();
    console.log("hideLoadingModal");
}

function showRetryAlert() {
    var modal = document.getElementById("retry-alert"); console.log(modal);
    modal.show();
    console.log("showRetryAlert");
}

function hideRetryAlert() {
    var modal = document.getElementById("retry-alert");
    modal.hide();
    console.log("hideRetryAlert");

    requestDashboardStat();
}

// https://css-tricks.com/snippets/jquery/draggable-without-jquery-ui/
// (function($) {
//     alert('drags');
//     $.fn.drags = function(opt) {

//         console.log('drags');

//         opt = $.extend({handle:"",cursor:"move"}, opt);

//         // if(opt.handle === "") {
//         //     var $el = this;
//         // } else {
//         //     var $el = this.find(opt.handle);
//         // }

//         // return $el.css('cursor', opt.cursor).on("mousedown", function(e) {
//         //     if(opt.handle === "") {
//         //         var $drag = $(this).addClass('draggable');
//         //     } else {
//         //         var $drag = $(this).addClass('active-handle').parent().addClass('draggable');
//         //     }
//         //     var z_idx = $drag.css('z-index'),
//         //         drg_h = $drag.outerHeight(),
//         //         drg_w = $drag.outerWidth(),
//         //         pos_y = $drag.offset().top + drg_h - e.pageY,
//         //         pos_x = $drag.offset().left + drg_w - e.pageX;
//         //     $drag.css('z-index', 1000).parents().on("mousemove", function(e) {
//         //         $('.draggable').offset({
//         //             top:e.pageY + pos_y - drg_h,
//         //             left:e.pageX + pos_x - drg_w
//         //         }).on("mouseup", function() {
//         //             $(this).removeClass('draggable').css('z-index', z_idx);
//         //         });
//         //     });
//         //     e.preventDefault(); // disable selection
//         // }).on("mouseup", function() {
//         //     if(opt.handle === "") {
//         //         $(this).removeClass('draggable');
//         //     } else {
//         //         $(this).removeClass('active-handle').parent().removeClass('draggable');
//         //     }
//         // });

//     }
// })(jQuery);

// $('div#campaign-stat').drags();

// https://www.kirupa.com/html5/drag.htm
// var dragItem = document.querySelector("#item");
// var container = document.querySelector("#container");
/* document.addEventListener('show', function(event) {
    var page = event.target;

    if (page.id === 'mapPage') {

        // [Violation] Added non-passive event listener to a scroll-blocking 'touchmove' event. Consider marking event handler as 'passive' to make the page more responsive.
        // $( "div#campaign-stat" ).mousemove(function( event ) {
        //     console.log('mousemove');
        //     var pageCoords = "( " + event.pageX + ", " + event.pageY + " )";
        //     var clientCoords = "( " + event.clientX + ", " + event.clientY + " )";
        //     $( "span:first" ).text( "( event.pageX, event.pageY ) : " + pageCoords );
        //     $( "span:last" ).text( "( event.clientX, event.clientY ) : " + clientCoords );
        // });

        // $( "div#campaign-stat" ).mousedown(function( event ) {
        //     console.log('mousedown');
        // });

        // setTimeout(function(){ 
        //     var container = document.querySelector("#campaign-stat");
        //     var dragItem = document.querySelector("#mapPage");

        //     var active = false;
        //     var currentX;
        //     var currentY;
        //     var initialX;
        //     var initialY;
        //     var xOffset = 0;
        //     var yOffset = 0;

        //     container.addEventListener("touchstart", dragStart, {passive: true});
        //     container.addEventListener("touchend", dragEnd, {passive: true});
        //     container.addEventListener("touchmove", drag, {passive: true});

        //     container.addEventListener("mousedown", dragStart, false);
        //     container.addEventListener("mouseup", dragEnd, false);
        //     container.addEventListener("mousemove", drag, false);

        //     function dragStart(e) {
        //         if (e.type === "mousedown") {
        //             initialX = e.touches[0].clientX - xOffset;
        //             initialY = e.touches[0].clientY - yOffset;
        //         } else {
        //             initialX = e.clientX - xOffset;
        //             initialY = e.clientY - yOffset;
        //         }

        //         if (e.target === dragItem) {
        //             active = true;
        //         }
        //     }

        //     function dragEnd(e) {
        //         initialX = currentX;
        //         initialY = currentY;

        //         active = false;
        //     }

        //     function drag(e) {
        //         if (active) {

        //             e.preventDefault();

        //             if (e.type === "mousemove") {
        //                 currentX = e.touches[0].clientX - initialX;
        //                 currentY = e.touches[0].clientY - initialY;
        //             } else {
        //                 currentX = e.clientX - initialX;
        //                 currentY = e.clientY - initialY;
        //             }

        //             xOffset = currentX;
        //             yOffset = currentY;

        //             setTranslate(currentX, currentY, dragItem);
        //         }
        //     }

        //     function setTranslate(xPos, yPos, el) {
        //         el.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
        //     }
        // }, 1000);

    }
}); */

document.addEventListener('init', function(event) {
    if (event.target.matches('#mapPage')) {
        // ons.notification.alert('mapPage is initiated using (2) document addEventListener init');
        // Set up content...
        console.trace();
        // setUpMap();

        dragElement(document.getElementById("campaign-stat"));
    }
}, false);



// KartaBase setUpCampaign
function setUpCampaign( campaign ) {
    var self = this;

    if(!campaign) return;

    var campaignId = campaign.campaign_id || -1;
    var campaignName = campaign.campaign_name || -1;
    var campaignUrl = campaign.campaign_photo || -1;
    localStorage.setItem( "karta-campaign-id", campaignId );
    localStorage.setItem( "karta-campaign-name", campaignName );
    localStorage.setItem( "karta-campaign-url", campaignUrl );
}



// https://www.w3schools.com/howto/howto_js_draggable.asp
var ystart = null;
var yend = null;
function dragElement(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById(elmnt.id + "header")) {
        /* if present, the header is where you move the DIV from:*/
        document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
        document.getElementById(elmnt.id + "header").ontouchstart = dragMouseDown;
    } else {
        /* otherwise, move the DIV from anywhere inside the DIV:*/
        elmnt.onmousedown = dragMouseDown;
        elmnt.ontouchstart = dragTouchDown;

        // https://developer.mozilla.org/en-US/docs/Web/API/Touch/pageX
        // Register a touchmove listeners for the 'source' element
        // var src = document.getElementById("source");

        // elmnt.addEventListener('touchmove', function(e) {
        //     // Iterate through the touch points that have moved and log each 
        //     // of the pageX/Y coordinates. The unit of each coordinate is CSS pixels.
        //     var i;
        //     for (i=0; i < e.changedTouches.length; i++) {
        //         console.log("touchpoint[" + i + "].pageX = " + e.changedTouches[i].pageX);
        //         console.log("touchpoint[" + i + "].pageY = " + e.changedTouches[i].pageY);
        //     }

        //     // elementDrag
        //     pos1 = pos3 - e.changedTouches[0].pageX;
        //     pos2 = pos4 - e.changedTouches[0].pageY;
        //     pos3 = e.changedTouches[0].pageX;
        //     pos4 = e.changedTouches[0].pageY;
        //     // set the element's new position:
        //     elmnt.style.top = (elmnt.offsetTop - pos2) + "px";

        //     pos3 = e.clientX;
        //     pos4 = e.clientY;
        //     // document.ontouchend = closeDragElement;
        //     // // call a function whenever the cursor moves:
        //     // document.ontouchmove = elementTouch;

        //     if (ystart == null) {
        //         ystart = pos4;
        //     }
        // }, false);
    }

    function dragMouseDown(e) {
        console.log("dragMouseDown");
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;

        if (ystart == null) {
            ystart = pos4;
        }
    }

    function dragTouchDown(e) {
        console.log("dragTouchDown");
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.changedTouches[0].pageX;
        pos4 = e.changedTouches[0].pageY;
        document.ontouchend = closeTouchElement;
        // call a function whenever the cursor moves:
        document.ontouchmove = elementTouch;

        if (ystart == null) {
            ystart = pos4;
        }
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        // elmnt.style.left = (elmnt.offsetLeft - pos1) + "px"; // disable drag left/right
    }

    function elementTouch(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.changedTouches[0].pageX;
        pos2 = pos4 - e.changedTouches[0].pageY;
        pos3 = e.changedTouches[0].pageX;
        pos4 = e.changedTouches[0].pageY;
        // set the element's new position:
        // elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        // elmnt.style.left = (elmnt.offsetLeft - pos1) + "px"; // disable drag left/right
        // var top = $('html').offset().top;
        // $('#campaign-stat').position();
        var minTop = screen.height * .4; // because opened slider is 40vh top
        // console.log('elementTouch ', Math.max(elmnt.offsetTop - pos2, minTop)); // , ', top: ', elmnt.top, ', pageYOffset: ', top); // elmnt.pageYOffset
        elmnt.style.top = (Math.max(elmnt.offsetTop - pos2, minTop)) + "px";
    }

    function closeDragElement() {
        /* stop moving when mouse button is released:*/
        document.onmouseup = null;
        document.onmousemove = null;

        // added by novi
        e = window.event;
        e.preventDefault();
        yend = e.clientY;

        // alert('ystart = ' + ystart + ', yend = ' + yend);
        slidePanel(); // new function to open/close slider
    }

    function closeTouchElement() {
        /* stop moving when mouse button is released:*/
        document.ontouchend = null;
        document.ontouchmove = null;

        // added by novi
        e = window.event;
        e.preventDefault();
        // yend = e.clientY;
        yend = e.changedTouches[0].pageY;
        // alert('ystart = ' + ystart + ', yend = ' + yend);
        slidePanel(); // new function to open/close slider
    }

    function slidePanel() {
        var ydiff = yend - ystart; // y larger value = bottom

        // add/remove class
        var campaignStat = $('#campaign-stat');
        if ($(campaignStat).hasClass("stat-first")) {
            $(campaignStat).removeClass("stat-first");
        }
        $(campaignStat).removeAttr("style");
        if (ydiff > 0) { // if swipe down then close panel
            // close panel if show
            if (!($(campaignStat).hasClass("stat-close"))) {
                $(campaignStat).addClass("stat-close");
            }
            console.log('close stat');
        }
        else { // if swipe up then open panel
            // open panel if hidden
            if ($(campaignStat).hasClass("stat-close")) {
                $(campaignStat).removeClass("stat-close");
            }
            console.log('open stat');
        }

        ystart = null;
        yend = null;
    }
}