
var KartaBase = {
    getVersion: function(){
        var self = this;
        if( "cordova" in window ) return BuildInfo.version
        console.log(BuildInfo.version);
        return KartaConfig.version;
    },
    getMenus: function( menu ){
        var self = this;

        var app = self.getApp();
        return app.shadowRoot.querySelector( "#main-selector" );
    },
    getCordovaPath: function( destination ){
        if( "cordova" in window ){
            return cordova.file.applicationDirectory + "www/" + destination;
        }

        return destination;
    },

    downloadUrl: function( url, filename ){
        
            requestFileSystem( LocalFileSystem.PERSISTENT, 0, function(fs){ 
                var ft = new FileTransfer;
                var fname = encodeURI("///storage/emulated/0/Download/" + filename);
                ft.download( encodeURI(url), fname, function(ev){
                }, function(err){
                    console.error( "error: ", err.source, err.target, err.code );
                } );
            } );
    },
    
    getGeoLiveRequest: function(){
        var self = this;
        var live = KartaBase.getDashboardLive(); 
        return live.shadowRoot.querySelector( "#geolive-ajax" );
    },
    getGeoJsonRequest: function(){
        var heatmap = KartaBase.getDashboardHeatmap(); 
        return heatmap.shadowRoot.querySelector( "#geojson-ajax" );
    },
    getMainQueue: function(){
        var self = this;
        var app = self.getApp();
        return app.shadowRoot.querySelector( "main-queue" );
    },
    getQueue: function(){
        return new Queue();
    },
    getMCDReportDownloadUrl: function( mcdId, campaignId, dateFrom, dateTo ){
        var reportUrl = KartaConfig.reportMCDUrl;
        return reportUrl+"id="+mcdId+"&campaign="+campaignId+"&date_from="+dateFrom+"&date_to="+dateTo;
    },
    setUpMCDInfo: function( mcdInfo ){
        var self = this;

        var app = self.getApp();
        var mcd = app.shadowRoot.querySelector( "karta-mcd" );
        mcd.mcdInfo = mcdInfo;
    },
    setUpGallery: function( data ){
        var self = this;

        var app = self.getApp();
        var det = app.shadowRoot.querySelector( "karta-gallery" );
        det.galleryData = data;
    },
    getBAPPReportDownloadUrl: function( bappId, campaignId, dateFrom, dateTo ){
        var reportUrl = KartaConfig.reportBAPPUrl;
        return reportUrl+bappId+"?id="+bappId+"&campaign="+campaignId+"&date_from="+dateFrom+"&date_to="+dateTo;
    },
    setUpBAPPInfo: function( bappInfo ){
        var self = this;

        var app = self.getApp();
        var bapp = app.shadowRoot.querySelector( "karta-bapp" );
        bapp.bappInfo = bappInfo;
    },
    getDriverReportDownloadUrl: function( driverId, campaignId, dateFrom, dateTo ){
        var reportUrl = KartaConfig.reportDriverUrl;
        return reportUrl+"id="+driverId+"&campaign="+campaignId+"&date_from="+dateFrom+"&date_to="+dateTo;
    },
    setUpDriverInfo: function( driverInfo ){
        var self = this;

        var app = self.getApp();
        var driver = app.shadowRoot.querySelector( "karta-driver" );
        driver.driverInfo = driverInfo;
    },
    setUpDriverDetailInfo: function( driverDetailInfo, driverFlatDetail, driverEvaluation ){
        var self = this;

        var app = self.getApp();
        var driver_detail = app.shadowRoot.querySelector( "karta-driver-detail" );
        driver_detail.driverDetailInfo = driverDetailInfo;
        driver_detail.driverFlatDetail = driverFlatDetail;
        driver_detail.driverEvaluation = driverEvaluation;
    },
    getCampaignId: function(){
        var self = this;
        return localStorage.getItem( "karta-campaign-id" );
    },
    selectDashboardNavBottom: function(){
        var self = this;

        var nav = KartaBase.getDashboardNavBottom();
    },
    setDashboardNavBottom: function(bflag){
        var self = this;

        var nav = KartaBase.getDashboardNavBottom();
        if(bflag) {
            nav.removeAttribute( "hidden" );
            return nav.style.display = "block";
        }

        nav.setAttribute( "hidden", "" );
        return nav.style.display = "none";
    },
    getDashboardNavBottom: function(){
        var self = this;

        var app = self.getApp();
        return app.shadowRoot.querySelector( "#dashboard-nav-bottom" );
    },
    setUpCampaign: function( campaign ){
        var self = this;

        if(!campaign) return;
        
        // var app = self.getApp();
        // // localStorage.removeItem("flagLiveHeatmap");
        //    // localStorage.setItem("flagLiveHeatmap","flagon");
        // // var campaignImg = campaign.campaign_photo;
        // app.campaignId = campaign.campaign_id || -1;
        // app.campaignName = campaign.campaign_name || "";
        // app.campaignUrl = campaign.campaign_photo || "";
        
        var campaignId = campaign.campaign_id || -1;
        var campaignName = campaign.campaign_name || -1;
        var campaignUrl = campaign.campaign_photo || -1;
        localStorage.setItem( "karta-campaign-id", campaignId );
        localStorage.setItem( "karta-campaign-name", campaignName );
        localStorage.setItem( "karta-campaign-url", campaignUrl );

        
        // // console.log("url logo ", localStorage.getItem("client_logo_url"));
        // // var url_img = ""; //demokarta
        // var url_img = localStorage.getItem("client_logo_url");
        // if(url_img === "" )
        // {
        //     url_img = "android_asset/www/images/default_client.png";
        // }
        // var header = app.shadowRoot.querySelector( "#main-header" );
        // header._bgFront.style.backgroundImage = "url('"+ url_img +"')"; //main
        // // header._bgFront.style.backgroundImage = "url('"+ localStorage.getItem("client_logo_url") +"')";
        // // header._bgFront.style.backgroundImage = "url('https://s3-ap-southeast-1.amazonaws.com/karta-images/upload/adasiabali.png')";
        // header._bgFront.style.backgroundSize = "contain";
        // header._bgFront.style.backgroundRepeat = "no-repeat";
        // header._bgFront.style.backgroundPosition = "center";
        // var page = self.getPage();
        // var handler = KartaBase.getAppHandler();
        // handler.handleRoute( app, page );
        // var element = app.shadowRoot.querySelector('#my-element');
        // $(element).textfill({

        // }); 
    },
    setPage: function( page ){
        var self = this;
        /* DEPRECATED since we are using AppHandlerhandlerRoute */

        /*
        var pages = self.getPages();
        pages.selected = page;

        setTimeout( function(){
            var app = self.getApp();
            var selector = app.shadowRoot.querySelector( "#main-selector" );
            selector.selected = page;

            / * First Initialization * /
            switch( page ){
                case "dashboard":
                    var handler = self.getDashboardHandler();
                    handler.setUpInitialDashboard();
                    break;
                    
                case "mcd":
                    var selected = app.shadowRoot.querySelector( "karta-" + page );
                    var bcheck = !(selected.pageChanged)
                    selected.pageChanged = bcheck;        
                    break;

                case "bapp":
                    var selected = app.shadowRoot.querySelector( "karta-" + page );
                    var bcheck = !(selected.pageChanged)
                    selected.pageChanged = bcheck;        
                    break;

                case "driver":
                    var selected = app.shadowRoot.querySelector( "karta-" + page );
                    var bcheck = !(selected.pageChanged)
                    selected.pageChanged = bcheck;        
                    break;
            }

        }, 3000 );
        */
    },
    // getPage: function(){
    //     var self = this;

    //     var pages = self.getPages();
    //     return pages.selected;
    // },
    getRequest: function(){
        var self = this;

        var app = self.getApp();
        return app.shadowRoot.querySelector( "#data-ajax" );
    },
    getDashboardNavBottomListener: function(){
        return new NavBottomListener();
    },
    getDashboardLiveRequest: function(){
        return new DashboardLiveRequest();
    },
    getDashboardHeatmapRequest: function(){
        return new DashboardHeatmapRequest();
    },
    getDashboardStatRequest: function(){
        return new DashboardStatRequest();
    },
    getDashboardHandler: function(){
        return new DashboardHandler();
    },
    // getPages: function(){
    //     var self = this;
    //     var app =  self.getApp();
    //     return app.shadowRoot.querySelector( "iron-pages" );
    // },
    getDashboardLive: function(){
        var self = this;
        var app = self.getApp();
        return app.shadowRoot.querySelector( "karta-dashboard-live" );
    },
    getDashboardHeatmap: function(){
        var self = this;
        var app = self.getApp();
        return app.shadowRoot.querySelector( "karta-dashboard-heatmap" );
    },
    getDashboard: function(){
        var self = this;
        var app = self.getApp();
        return app.shadowRoot.querySelector( "karta-dashboard" );
    },
    getAppHandler: function(){
        return new AppHandler();
    },
    getApp: function(){
        /* check if app is online */
        if(!navigator.onLine){
            /* STATUSBAR HERE */
            console.error( "Sorry, no connection available" );
        }

        return document.querySelector("karta-app");
    },
    startUI: function() {
        document.querySelector("#pd-loader").open();
    },
    finishUI: function() {
        document.querySelector("#pd-loader").close();   
    },

    // new onsen-ui
    getMain: function() {
        var self = this;

        return document.querySelector("ons-page#mainPage");
    },
    getPage: function(page) {
        // map, gallery, driver, myAccount
        var self = this;

        var main = KartaBase.getMain();
        var page = main.querySelector("ons-page#" + page + "Page");

        return page;
    }
};